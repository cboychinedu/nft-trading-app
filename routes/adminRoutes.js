// Importing the necessary modules 
const express = require('express'); 
const session = require('express-session'); 
const mongodbSession = require('connect-mongodb-session')(session); 
const bcrypt = require('bcrypt'); 
const path= require('path'); 
const { protectedRoute, registerNewAdmin } = require('../auth/auth'); 
const { ADMIN } = require('../models/validation'); 
const { rootPath } = require('../base'); 

// Creating the router object 
const router = express.Router();  

// Setting the routes for the admin home page 
router.get('/', async (req, res) => {
    // Checking for the user email address in the session storage 
    if (req.session._id) {
        // Redirect the user to the dashboard page 
        return res.redirect('/admin/dashboard')
    }

    // Setting the path to the html template file 
    let fullPath = path.join(rootPath, 'static', 'templates', 'admin.ejs'); 

    // Rendering the home page 
    return res.render(fullPath); 
})

// Setting the post login route for the admin page 
router.post('/login', async (req, res) => {

    // Verify the admin before logging the admin into the admin panel 
    try {
        // Searching if the admin already exists on the database 
        let admin = await ADMIN.findOne({
            emailAddress: req.body.emailAddress
        }); 

        // If the email address specified was not found 
        // on the database 
        if (!admin) {
            // Create the error message 
             let errorMessage = JSON.stringify({
                "message": "Invalid email or password.", 
                "status": "error", 
                "statusCode": 404, 
            })

            // Send back the error message 
            return res.send(errorMessage).status(404);
        }

        // If the email address was found on the database server 
        else {
            // Execute the block of code below 
            let adminPassword = req.body.password; 
            let hashedPassword = admin.password; 

            // Comparing the password to see if is valid 
            let passwordCondition = await bcrypt.compare(adminPassword, hashedPassword);
            
            // Sending back a response if the password is validated 
            if (passwordCondition) {
                // Creating the admin session object and place it into the request header 
                sess = req.session; 
                sess.emailAddress = req.body.emailAddress; 
                sess._id = admin._id; 
                sess.isAuth = true; 

                // Sending the response for the successful connection to the database 
                let successMessage = JSON.stringify({
                    "message": "Admin user logged in", 
                    "status": "success", 
                    "statusCode": 200, 
                })

                // Sending back the success message 
                return res.send(successMessage).status(successMessage["statusCode"]); 
            }

            // If the password is not validated 
            else {
                // For the password not validated 
                let errorMessage  = JSON.stringify({
                    "message": "Invalid email or password", 
                    "status": "error", 
                    "statusCode": 404, 
                }); 

                // Sending back the error message 
                return res.send(errorMessage).status(errorMessage["statusCode"]); 
            }

        }
    }

    // On error execute the block of code below 
    catch (error) {
        // On error connecting to the database, execute the block of code 
        // below 
        let errorMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error", 
            "statusCode": 500, 
        })

        // Sending back the error message 
        return res.send(errorMessage).status(500); 
    }
})

// Setting the route for register route 
router.post('/register', registerNewAdmin, async(req, res) => {
    // Searching the database to see if the user with the specified email address is 
    // already registered on the database 
    let newUser = await ADMIN.findOne({
        "emailAddress": req.body.emailAddress
    }); 

    // If the newUser is not empty execute the block of code below 
    if (newUser) {
        // If the email is found on the database, execute the block of code below 
        let errMessage = JSON.stringify({
            "message": "Admin user already registered on the database", 
            "status": "error", 
            "statusCode": 500, 
        }); 

        // Sending the json message 
        return res.send(errMessage); 
    }

    // If the email for the user is not found, execute the block of code below 
    else {
        // Encrypt the password, connect to the database and save the user 
        let salt = await bcrypt.genSalt(6); 
        hashedPassword = await bcrypt.hash(req.body.password, salt); 

        // Saving the new registered user 
        let registeredUser = new ADMIN({
            firstname: req.body.firstname, 
            lastname: req.body.lastname, 
            emailAddress: req.body.emailAddress, 
            password: hashedPassword, 
        })

        // Saving the user on the data base 
        try{
            // Saving the registered results on the database 
            let result = await registeredUser.save(); 

            // Create a success message, and send it back to the client 
            let successMessage = JSON.stringify({
                "message": "Admin user registered on the database", 
                "status": "success", 
                "statusCode": 200, 
            }); 

            // Sending back the success message 
            return res.send(successMessage).status(200); 
        }

        // On extended error, execute the block of code below 
        catch (error) {
            // On generated errors, log them and save to disk 
            // Create the error message, and send it back to the user 
            let errorMessage = JSON.stringify({
                "message": error.toString().trim(), 
                "status": "error", 
                "statusCode": 500, 
            })

            // Sending the error message 
            return res.send(errorMessage).status(500); 
        }
    }
})

// Creating the route for the admin dashboard
router.get('/dashboard', async(req, res) => {
    // Checking for the user email address 
    if (req.session._id) {
        // Getting the information about the logged in user by checking 
        // the database for the existing user 
        try {
            // Getting the admin users 
            const adminUsers = await ADMIN 
            .findOne({
                emailAddress: req.session.emailAddress
            })
            .select({
                emailAddress: 1, firstname: 1, lastname: 1, 
            })

            // If the user data exists on the database, execute the block of code below 
            // and send back the data to the user's 
            if (adminUsers) {
                // If the adminUser data is authentic 
                // Setting the path to the dashboard html template file 
                let fullPath = path.join(rootPath, 'static', 'templates', 'adminPages', 'adminDashboard.ejs'); 

                // Rendering the dashboard page 
                return res.render(fullPath, {
                    firstname: adminUsers.firstname, 
                    lastname: adminUsers.lastname, 
                    emailAddress: adminUsers.emailAddress
                })
            }

            // Else if the user's data is not authentic, execute the block of code below 
            else {
                // Redirect the user back to the home page 
                // Getting the full path to the page 
                let fullPath = path.join(rootPath, 'static', 'templates', 'admin.ejs'); 

                // Rendering the admin home page 
                return res.render(fullPath); 
            }

        }

        // Catch any error generated 
        catch (error) {
            // On errors connecting to the data base, execute the block of code below 
            let fullPath = path.join(rootPath, 'static', 'templates', 'admin.ejs'); 

            // Rendering the home page 
            return res.render(fullPath); 
        }
    }

    // Else if the user's session is not available, execute the block of code below 
    else {
        // If the user session does not exists inside the session storage, execute the code
        // below
        let fullPath = path.join(rootPath, 'static', 'templates', 'admin.ejs')

        // Rendering the home page 
        return res.render(fullPath);
    }
})

// Creating a route for handling different route parameters 
router.get('/:routeParams', protectedRoute, async (req, res) => {
    // Getting the information about the logged in user by checking 
    // the database for the existing user 

    try {
        // Getting the admin user data 
        const adminData = await ADMIN
        .findOne({
            _id: req.session._id 
        })
        .select({
            emailAddress: 1, firstname: 1, lastname: 1, 

        })

        // If the admin user data exists on the database, execute the block of 
        // code below 
        if (adminData) {
            // Checking out the specified route parameters 
            const routeParams = req.params.routeParams; 

            // if the route params is Dashboard for the admin, execute 
            // the block of code below 
            if (routeParams === "adminDashboard") {
                // Setting the path to the html template file 
                let fullPath = path.join(rootPath, 'static', 'templates', 'adminPages', 'adminDashboard.ejs'); 

                // Rendering the admin dashboard page 
                return res.render(fullPath, {
                    firstname: adminData.firstname, 
                    lastname: adminData.lastname, 
                    emailAddress: adminData.emailAddress, 
                })
            }

            // Else if the route params is Nft Market place 
            else if (routeParams === "nftMarketPlace") {
                // Setting the path to the ejs template file for admin nftMarketplace 
                let fullPath = path.join(
                    rootPath, 'static', 'templates', 
                    'adminPages', 'adminNftMarketPlace.ejs'
                ); 

                // Rendering the nftMarketplace template file/page 
                return res.render(fullPath, {
                    firstname: adminData.firstname, 
                    lastname: adminData.lastname, 
                    emailAddress: adminData.emailAddress,
                })
            }

            // Else if the route params is Upload NFT 
            else if (routeParams === "uploadNft") {
                // Setting the path to the ejs template file for the admin upload nft 
                let fullPath = path.join(
                    rootPath, 'static', 'templates', 
                    'adminPages', 'adminUploadNft.ejs'
                )

                // Rendering the Upload NFT template file/page 
                return res.render(fullPath, {
                    firstname: adminData.firstname, 
                    lastname: adminData.lastname, 
                    emailAddress: adminData.emailAddress,
                })
            }

            // Else if the route params is Message 
            else if (routeParams === "messages") {
                // Setting the path to the ejs template file for the admin user to 
                // message registered user's 
                let fullPath = path.join(
                    rootPath, 'static', 'templates', 
                    'adminPages', 'adminMessages.ejs'
                )

                // Rendering the messages template file/page 
                return res.render(fullPath, {
                    firstname: adminData.firstname, 
                    lastname: adminData.lastname, 
                    emailAddress: adminData.emailAddress,
                })
            }

            // Else if the route params is Transactions 
            else if (routeParams === "transaction") {
                // Setting the path to the ejs template file for the transaction page 
                let fullPath = path.join(
                    rootPath, 'static', 'templates', 
                    'adminPages', 'adminTransactions.ejs'
                )

                // Rendering the Transactions template file/page 
                return res.render(fullPath, {
                    firstname: adminData.firstname, 
                    lastname: adminData.lastname, 
                    emailAddress: adminData.emailAddress,
                })
            }

            // Else if the route params is My Wallet 
            else if (routeParams === "myWallet") {
                // Setting the path to the ejs template file for my wallet page 
                let fullPath = path.join(
                    rootPath, 'static', 'templates', 
                    'adminPages', 'myWallet.ejs'
                )

                // Rendering the MyWallet template file/page 
                return res.render(fullPath, {
                    firstname: adminData.firstname, 
                    lastname: adminData.lastname, 
                    emailAddress: adminData.emailAddress,
                })
            }

            // Else if the route params is Analytics 
            else if (routeParams === "analytics") {
                // Setting the path to the ejs template file for anaytics page 
                let fullPath = path.join(
                    rootPath, 'static', 'templates', 
                    'adminPages', 'adminAnalytics.ejs'
                )

                // Rendering the Analytics template file/page 
                return res.render(fullPath, {
                    firstname: adminData.firstname, 
                    lastname: adminData.lastname, 
                    emailAddress: adminData.emailAddress,
                })
            }

            // Else if the route params is Settings 
            else if (routeParams === "settings") {
                // Setting the path to the ejs template file for settings page 
                let fullPath = path.join(
                    rootPath, 'static', 'templates', 
                    'adminPages', 'adminSettings.ejs'
                )

                // Rendering the Settings template file/page 
                return res.render(fullPath, {
                    firstname: adminData.firstname, 
                    lastname: adminData.lastname, 
                    emailAddress: adminData.emailAddress,
                })
            }

            // Else if the route params is Profile 
            else if (routeParams === "profile") {
                // Setting the path to the ejs template file for the profile page 
                let fullPath = path.join(
                    rootPath, 'static', 'templates', 
                    'adminPages', 'adminProfile.ejs'
                )

                // Rendering the admin profile template file/page 
                return res.render(fullPath, {
                    firstname: adminData.firstname, 
                    lastname: adminData.lastname, 
                    emailAddress: adminData.emailAddress,
                })

            }
        }

    }

    // Catch the error connecting to the data base 
    catch (error) {
        // Error connecting to the database, redirect the user back to the home page 
        let fullPath = path.join(rootPath, 'static', 'templates', 'admin.ejs'); 

        // Rendering the home template 
        return res.render(fullPath); 
    }
})

// Setting the logout route 
router.get('/logout', async(req, res) => {

    // Destroying the user's sessions 
    req.session.destroy((error) => {
        if (error) {
            // Throw an error if there was an error destroying the user's session 
            console.log(error); 

            // Redirect the user to the home page route 
            return res.redirect('/admin'); 
        }

        // On removal of the session cookie data, redirect the user to the home route. 
        else {
            // Assign the session value of the null type 
            req.session = null; 

            // Setting a delay function for 2 seconds 
            setTimeout(() => {
                // Redirect the user to the home page 
                return res.redirect('/admin')
            }, 2000); 
            

        }
    }); 
    
});



// Exporting the router object 
module.exports = router; 
