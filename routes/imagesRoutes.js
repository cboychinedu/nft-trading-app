// Importing the necessary modules 
const fs = require('fs'); 
const express = require('express'); 
const path = require('path'); 
const { protectedRoute } = require('../auth/auth');
const { rootPath } = require('../base'); 

// Creating the router object 
const router = express.Router(); 

// Creating the session variable 
let sess; 

// Setting the routes for the home page 
router.get('/:filename', protectedRoute, async (req, res) => {
    // Getting the file name 
    const filename = req.params.filename; 

    // Getting the file path 
    const fullPath = path.join(rootPath, 'static', 'uploads', 'imageData', filename)

    // Redering the image 
    return res.sendFile(fullPath); 
    
})

// Creating a route for the land 
router.get("/marketplace/:filename", protectedRoute, async (req, res) => {
    // Getting the file name 
    const filename = req.params.filename; 

    // Getting the fullpath 
    const fullpath = path.join(rootPath, 'static', 'uploads', filename); 

    // Rendering the file 
    return res.sendFile(fullpath); 

})

// Exporting the router object 
module.exports = router; 