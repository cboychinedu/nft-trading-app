// Importing the necessary modules 
const fs = require('fs'); 
const express = require('express'); 
const session = require('express-session'); 
const mongodbSession = require('connect-mongodb-session'); 
const path = require('path'); 
const { rootPath } = require('../base'); 
const { NFTFILE, ADMIN } = require('../models/validation')

// Creating the router object 
const router = express.Router(); 

// Setting the routes for the nft data 
router.get("/getNft", async (req, res) => {
    // Getting the uploaded blob files 
    let nftData = await NFTFILE.find()
    // console.log(nftData); 

    return res.send(JSON.stringify(nftData)); 
}); 

// Setting a route for creating a new nft file
router.post('/createNft', async (req, res) => {
    // Using try catch block 
    try {
        // Getting the information about the logged in admin user by 
        // checking the database 
        const adminUser = await ADMIN.findOne({
            _id: req.session._id
        })
        .select({
            emailAddress: 1, 
            phoneNumber: 1, 
            firstname: 1, 
            lastname: 1, 
            accountBalance: 1, 
            date: 1
        })

        // If the admin user data is present on the database, then 
        // execute the block of code below 
        if (adminUser) {
            // Getting the uploaded nft file 
            let image = req.file.myFile; 
            let imageName = req.files.myFile.name;

            // Adding data to the image 
            imageName = Date().split("GMT")[0] + imageName; 
            imageName = String(imageName); 

            // Setting the path to the image 
            const imageFullPath = path.join(rootPath, 'static', 'uploads', imageName); 

            // Moving the uploaded image to the specified path 
            image.mv(imageFullPath, async (error, result) => {
                // If the error is present then execute the 
                // block of code below 
                if (error) {
                    // Specify the context type header as 'application/json' file format 
                    res.writeHead(500, { 'Content-Type': 'application/json' }); 

                    // Creat a success message 
                    let successMessaage = JSON.stringify({ status: 'error', message: 'Failed to upload the image file'}); 

                    // Sending the error back tot he client 
                    return res.end(successMessaage);
                }

                // Else 
                else {
                    // Specify the content type header as 'application/json' file format 
                    res.writeHead(200, { 'Content-Type': 'application/json' });

                    // Saving the new create nft collection 
                    let uploadedNftCollection = new NFTFILE({
                        firstname: adminUser.firstname, 
                        lastname: adminUser.lastname, 
                        emailAddress: adminUser.emailAddress, 
                        nftTitle: req.body.nftTitle,
                        nftArtistName: req.body.nftArtistName,
                        nftCategory: req.body.nftCategory,
                        nftDescription: req.body.nftDescription,
                        nftBlobFileLocation: "null",
                        nftImageFileLocation: imageFullPath,
                        nftPrice: req.body.nftPrice,
                    })

                    // Saving the new nft data to the database 
                    await uploadedNftCollection.save(); 

                    // Create a success message, and send it back to the client 
                    let successMessage = JSON.stringify({
                        "status": "success",
                        "message": "Nft was successfully uploaded", 
                        "status_code": 200, 
                    }); 

                    // Sending the success message back to the client
                    return res.end(successMessage);
                }
            })

        }


    }

    // Getting the error message resuting from the server
    catch(error) {
        // On generated error, send it back to the client
        let errorMessage = JSON.stringify({
            "status": "error",
            "message": error.toString(), 
            "statusCode": 500, 
        }); 

        // Sending the error message back to the client
        return res.send(errorMessage);
    }


})



// Exporting the nft data 
module.exports = router; 