// Importing the necessary modules 
const express = require('express'); 
const { rootPath } = require('../base'); 
const { protectedRoute } = require('../auth/auth');
const { NFTFILE } = require('../models/validation')

// Creating the router object 
const router = express.Router(); 

// Setting the routes for the nft data 
router.get("/", protectedRoute, async (req, res) => {
    // Getting the uploaded blob files 
    let nftData = await NFTFILE.find()

    // Sending the nft data 
    return res.send(JSON.stringify(nftData)); 
})

// Setting the route for searching the nft data 
router.post('/searchNft', protectedRoute, async (req, res) => {
    // Gettting the request body 
    const searchData = req.body.searchData; 

    // Searching for the data 
    let nftData = await NFTFILE.find({
        emailAddress: searchData
    })

    // Sending the nft data 
    nftData = JSON.stringify(nftData); 
    return res.send(nftData); 
})


// Exporting the nft data 
module.exports = router; 