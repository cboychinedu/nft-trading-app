// Importing the necessary modules
const express = require('express');
const { MAINCARTITEM } = require('../models/mainCart'); 
const bcrypt = require('bcrypt');
const path = require('path');
const { protectedRoute } = require('../auth/auth');
const { USERS, NFTFILE } = require('../models/validation');
const { rootPath } = require('../base');

// Creating the router object
const router = express.Router();

// Creating the session variable
let sess;

// Creating a list to hold the cart items 
let cartItems = []; 

// Creating a route for calculating the total prices of 
// the cart 
router.post('/calculateCartPrice', protectedRoute, async(req, res) => {
    // Getting the informatio about the carts 
    const cartId = req.body.cartId; 

    // Creating a list to hold the price 
    const cartTotalPrice = []

    // Using try catch block to extract the cartId for a specific 
    // cart 
    try {
        // Getting all the cart data from a specific body parameter 
        const cartData = await MAINCARTITEM.findOne({
            _id: cartId, 
        })

        // Adding the price 
        cartData.items.forEach((items) => {
            // Getting the price 
            let itemsPrice = items.nftPrice; 

            // Converting the price into a number 
            itemsPrice = Number.parseFloat(itemsPrice); 

            // Saving the items price into the list above 
            cartTotalPrice.push(itemsPrice); 
        })

        // Calculating the sum of the prices for each cart in ether 
        let sum = cartTotalPrice.reduce((acc, currentValue) => acc + currentValue, 0); 

        // Rounding off to 3dp 
        sum = sum.toFixed(3); 
        
        // Building the success message 
        let successMessage = JSON.stringify({
            "message": "Total sum of cart added",
            "cartSubTotal": sum,
            "cartDiscount": 0.345, 
            "cartTotal": sum,   
            "status": "success", 
            "statusCode": 200, 
        })

        // Sending the success Message 
        return res.send(successMessage).status(200); 

    }

    // Catching the error 
    catch(error) {
        // On error connecting to the database, execute the block of code 
        // below 
        let errorMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error", 
            "statusCode": 500, 
        })

        // Sending back the error message 
        return res.send(errorMessage).status(500); 
    }
})

// Creating a route for cart 
router.post('/addCart', protectedRoute, async(req, res) => {

    // Using try catch block 
    try {
        // Getting the request data from the request body 
        const requestData = req.body;

        // Create a new collection for the cart data to hold 
        // the request data as a list 
        const newItem = new MAINCARTITEM({
            items: requestData.data
        })

        // Save the new cart data 
        const result = await newItem.save(); 

        // Return the saved result 
        return res.send(result); 
    }

    // Catch 
    catch (error) {
        // On error connecting to the database, execute the block of code 
        // below 
        let errorMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error", 
            "statusCode": 500, 
        })

        // Sending back the error message 
        return res.send(errorMessage).status(500); 

    }


})

// Creating a route for viewing the carts 
router.post('/viewCart/:routeParams', protectedRoute, async (req, res) => {
    // Getting the information about the carts from the 
    // route parameter 
    const routeParms = req.params.routeParams; 

    // Using try catch block to extract the id for a 
    // specific cart 
    try {
        // Getting all the cart data from a specific route 
        // parameters 
        const cartData = await MAINCARTITEM.findOne({
            _id: routeParms, 
        })

        // if the cart data exists 
        if (cartData) {
            return res.send(cartData)
        }

        else {
            // Creating the error message 
            let errorMessage = JSON.stringify({
                "message": "No cart data found", 
                "status": "error", 
                "statusCode": 401, 
            })

            // Sending the error message 
            return res.send(errorMessage)
        }
    }

    // Catch 
    catch (error) {
        // On generated errors, log them and save to disk
        // create the error message, and send it back to the user
        let errorMessage = JSON.stringify({
                "message": error.toString().trim(),
                "status": "error",
                "statusCode": 500,
        });

        // Sending the error message
        return res.send(errorMessage).status(500);

    }
})

// Creating GET route for displaying the selected carts from 
// the database 
router.get('/viewCart/:routeParams', protectedRoute, async (req, res) => {
    // Getting the information about the logged in user 
    try {
        // Getting the email address from the user Sessions 
        const userData = await USERS
        .findOne({
            emailAddress: req.session.emailAddress
        })
        .select({
            emailAddress: 1, phoneNumber: 1, firstname: 1, lastname: 1,
            accountBalance: 1, date: 1,

        })

        // If the user data exists on the database 
        if (userData) {
            // Setting the path to the html template file 
            // Getting the information about the carts from the 
            // route parameter 
            const routeParms = req.params.routeParams; 

            // Getting the full path to the viewCart.ejs file 
            let fullPath = path.join(
                rootPath, 'static', 'templates', 
                'dashboardPages', 'viewCart.ejs'
            )

            // Rendering the file 
            return res.render(fullPath, {
                firstname: userData.firstname,
                lastname: userData.lastname,
                emailAddress: userData.emailAddress,
                accountBalance: userData.accountBalance,
                date: userData.date,
            }); 
        }

        // else if the user's data is not present on the database
        else {
            // redirecting the user back to the home page
            // Getting the full path to the page
            let fullPath = path.join(rootPath, 'static', 'templates', 'login.ejs')
            return res.render(fullPath);
        }

    }

    // Catch 
    catch (error) {
        // Error connecting to the database, redirect the user back to the home page
        let fullPath = path.join(rootPath, 'static', 'templates', 'login.ejs')
        return res.render(fullPath);
    }

})

// Creating a route for changing the password
router.put('/changePassword', protectedRoute, async (req, res) => {
    // Checking if the user session id is valid
    if (req.session._id) {
        // If the user is logged in, execute the block of code
        // below
        try {
            // Getting the user's data from the emailAddress
            // specified
            const userData = await USERS.findOne({
                emailAddress: req.session.emailAddress
            })
            .select({
                password: 1
            })

            // If the user data exists on the database, execute the block of
            // code below
            if (!userData) {
                // IF the user data was not found on the database, execute the line
                // of code below
                let errorMessage = JSON.stringify({
                    "message": "User not found on the database",
                    "status": "error",
                    "statusCode": 400,
                })

                // Send back the error message
                return res.send(errorMessage).status(errorMessage["statusCode"])

            }

            // Else if the user data is present
            else {
                // Execute the block of code below
                let newPassword = req.body.newPassword;
                let oldPassword = req.body.oldPassword;
                let oldPasswordHash = userData.password;

                // Comparing the password to see if it is valid
                let passwordCondition = await bcrypt.compare(oldPassword, oldPasswordHash);


                // Checking if the specified old password, and the user typed old password is valid
                if (passwordCondition) {
                    // Converting the password into a hash value
                    let salt = await bcrypt.genSalt(5)
                    let password = await bcrypt.hash(newPassword, salt);

                    // Saving the new password
                    userData.password = password;

                    // Saving the user on the data base
                    try {
                        // Saving the new password to the database
                        await userData.save();

                        // Create a success message, and send it back to the cline
                        let successMessage = JSON.stringify({
                            "message": "Password changed",
                            "status": "success",
                            "statusCode": 200,
                        })

                        // Sending back the success message
                        return res.send(successMessage).status(200);
                    }

                    // Catch
                    catch (error) {
                        // On generated errors, log them and save to disk
                        // Create the error message, and send it back to the user
                        let errorMessage = JSON.stringify({
                            "message": error.toString().trim(),
                            "status": "error",
                            "statusCode": 500,
                        })

                        // Sending the error message
                        return res.send(errorMessage).status(500)

                    }
                }

                // else if the password condtion return a false value
                else {
                    // For the password not validated, execute the block of
                    // code below
                    let errorMessage = JSON.stringify({
                        "message": "Password not correct",
                        "status": "error",
                        "statusCode": 404,
                    })

                    // Sending back the error message
                    return res.send(errorMessage).status(errorMessage['statusCode'])
                }

            }
        }

        // Catch
        catch (error) {
            // On error connecting to the database, execute the block of code
            // below
            let errorMessage = JSON.stringify({
                "message": error.toString().trim(),
                "status": "error",
                "statusCode": 500,
            })

            // Sending back the error message
            return res.send(errorMessage).status(500)

        }

    }

    // If the session is not present in the session id, execute the block
    // of code below
    else {
        // Creating the error message
        let errorMessage = JSON.stringify({
            "message": "User not logged in",
            "status": "error-redirect",
            "statusCode": 400
        })

        // Sending the error message
        return res.send(errorMessage).status(500);
    }

})

// Creating a route for editing the user's profile
router.post('/editProfile', protectedRoute, async (req, res) => {
    // Checking for the user email address
    if (req.session._id) {
        // If the user is logged in, execute the block of code below
        try {
            // Getting the user's data
            let userData = await USERS.findOneAndUpdate({
                "_id": req.session._id
            }, {
                $set: {
                    firstname: req.body.firstname || userData.firstname,
                    lastname: req.body.lastname || userData.lastname,
                    phoneNumber: req.body.phoneNumber || userData.phoneNumber,
                    emailAddress: req.body.emailAddress || userData.emailAddress,
                    countryOfResidence: req.body.countryOfResidence || userData.countryOfResidence
                }
            });

            // IF the user data exists on the databaes, execute the block of code below
            if (userData) {

                // Saving the user's data on the database
                try{
                    // Creating a success message, and send it back to the client
                    let successMessage = JSON.stringify({
                        "message": "User information changed",
                        "status": "success",
                        "statusCode": 200,
                    });

                    // Sending back the success message
                    return res.send(successMessage).status(200);
                }
                // On error
                catch (error) {
                    // Creating the error message
                    let errorMessage = JSON.stringify({
                        "message": error.toString().trim(),
                        "status": "error",
                        "statusCode": 500,
                    });

                    // Sending back the success message
                    return res.send(errorMessage).status(500);
                }
            }

            else {
                // Creating the error message
                let errorMessage = JSON.stringify({
                    "message": "User not found on the database",
                    "status": "error",
                    "statusCode": 400
                })

                // Sending the error message
                return res.send(errorMessage).status(500);

            }
        }

        // Catching the error message, and returning it back to the user
        catch (error) {
            // Creating the error message
            let errorMessage = JSON.stringify({
                "message": error.toString().trim(),
                "status": "error",
                "statusCode": 500,
            });

            // Sending back the success message
            return res.send(errorMessage).status(500);
        }
    }

    // If the session is not present in the session id, execute the block
    // of code below
    else {
        // Creating the error message
        let errorMessage = JSON.stringify({
            "message": "User not found on the database",
            "status": "error",
            "statusCode": 400
        })

        // Sending the error message
        return res.send(errorMessage).status(500);
    }
})

// Setting the routes for the dashboard home page
// protectedRoute,
router.get('/', async (req, res) => {
    // Checking for the user email address
    if (req.session._id) {
        // Getting the information about the logged in user by checking the
        // database for the existing user
        try {
            const userData = await USERS
            .findOne({
                emailAddress: req.session.emailAddress
            })
            .select({
                emailAddress: 1, phoneNumber: 1, firstname: 1, lastname: 1,
                accountBalance: 1, date: 1,

            })

            // If the user data exists on the database, execute the block of code below
            if (userData) {
                // Setting the path to the html template file
                let fullPath = path.join(rootPath, 'static', 'templates', 'dashboard.ejs');

                // Rendering the dashboard page
                return res.render(fullPath, {
                    firstname: userData.firstname,
                    lastname: userData.lastname,
                    emailAddress: userData.emailAddress,
                    accountBalance: userData.accountBalance,
                    date: userData.date,
                })
            }

            // else if the user's data is not present on the database
            else {
                // redirecting the user back to the home page
                // Getting the full path to the page
                let fullPath = path.join(rootPath, 'static', 'templates', 'login.ejs')
                return res.render(fullPath);
            }
        }
        catch(error) {
            // Error connecting to the database, redirect the user back to the home page
            let fullPath = path.join(rootPath, 'static', 'templates', 'login.ejs')
            return res.render(fullPath);
        }
    }

    else {
        // IF the user session does not exists inside the session storage, execute the code
        // below
        let fullPath = path.join(rootPath, 'static', 'templates', 'login.ejs')
        return res.render(fullPath);
    }
})

// Creating a route for creating nft collections
router.post('/:routeParams', protectedRoute, async(req, res) => {
    // Checking for the user email addrerss
    if (req.session._id) {
        // Getting the information about the logged in user by checking the
        // database for the existing user
        try {
            // Getting the information about the logged in user by checking
            // the database for the existing user
            const userData = await USERS.findOne({
                _id: req.session._id
            })
            .select({
                emailAddress: 1, phoneNumber: 1, firstname: 1, lastname: 1,
                accountBalance: 1, date: 1,

            })


            // if the user data exists on the database, execute the block of code below
            if (userData) {
                // Checking the route params
                const routeParams = req.params.routeParams;


                // if the route params is createNft
                if (routeParams === "createNft") {
                    // Getting the image and changing the name
                    let image = req.files.myFile;
                    let imageName = req.files.myFile.name;

                    // Adding date to the image
                    imageName = Date().split("GMT")[0] + imageName;
                    imageName = String(imageName);

                    // Setting the path to the image
                    const imageFullPath = path.join(rootPath, 'static', 'uploads', imageName);

                    // Moving the uploaded image to the specified path
                    image.mv(imageFullPath, async (error) => {
                        if (error) {
                            // Specify the context type header as 'application/json' file format
                            res.writeHead(500, {'Content-Type': 'application/json'})

                            // Create a success message
                            let successMsg = JSON.stringify({ status: 'error', message: 'Failed to upload image file'});

                            // Sending the error back to the client
                            return res.end(successMsg);
                        }

                        else {
                            // Specify the content type header as "application/json" file format
                            // res.writeHead(200, {'Content-Type': 'application/json' });

                            // Saving the new created nft collection
                            let uploadedNftCollection = new NFTFILE({
                                firstname: userData.firstname,
                                lastname: userData.lastname,
                                emailAddress: userData.emailAddress,
                                nftTitle: req.body.nftTitle,
                                nftArtistName: req.body.nftArtistName,
                                nftCategory: req.body.nftCategory,
                                nftDescription: req.body.nftDescription,
                                nftBlobFileLocation: "null",
                                nftImageFileLocation: imageFullPath,
                                nftPrice: req.body.nftPrice,
                            })

                                // Saving the nft collections
                            try {
                                    // Saving the nft collections on the database
                                    let result = await uploadedNftCollection.save();

                                    // Create a success message, and send it back to the client
                                    let successMessage = JSON.stringify({
                                        "message": "Nft collection saved on the database",
                                        "status": "success",
                                        "statusCode": 200,
                                    });

                                    // Sending back the success message
                                    return res.send(successMessage).status(200);
                            }

                            // Catch the error
                            catch (error) {
                                // On generated errors, log them and save to disk
                                // create the error message, and send it back to the user
                                let errorMessage = JSON.stringify({
                                        "message": error.toString().trim(),
                                        "status": "error",
                                        "statusCode": 500,
                                });

                                // Sending the error message
                                return res.send(errorMessage).status(500);
                            }
                        }

                    })
                }

            }

            // else if the user data was not found on the database
            else {
                // Creating error message
                let errorMessage = JSON.stringify({
                    "message": "User not found on the database",
                    "status": "error_user_not_found",
                    "statusCode": 500,
                });

                // Sending the error message
                return res.send(errorMessage).status(500);
            }
        }

        // Catch error
        catch (error) {
          // Creating error message
          let errorMessage = JSON.stringify({
            "message": error.toString().trim(),
            "status": "error",
            "statusCode": 500,
          })

          // Sending the error message
          return res.send(errorMessage).status(500);

        }
    }
})

// Creating a route for handling different route parameters
router.get('/:routeParams', protectedRoute, async (req, res) => {
    // Checking for the user email address
    if (req.session._id) {
        // Getting the information about the logged in user by checking the
        // database for the existing user
        try {
            const userData = await USERS
            .findOne({
                _id: req.session._id
            })
            .select({
                emailAddress: 1, phoneNumber: 1, firstname: 1, lastname: 1,
                accountBalance: 1, date: 1, phoneNumber: 1, countryOfResidence: 1,

            })

            // If the user data exists on the database, execute the block of code below
            if (userData) {
                // Checking the route params
                const routeParams = req.params.routeParams;

                // IF the route params is nftMarketPlace, execute the block of code below
                if (routeParams === "nftMarketPlace") {
                    // Setting the path to the html template file
                    let fullPath = path.join(rootPath, 'static', 'templates', 'dashboardPages', 'nftMarketPlace.ejs');

                    // Rendering the dashborad page
                    return res.render(fullPath, {
                        firstname: userData.firstname,
                        lastname: userData.lastname,
                        emailAddress: userData.emailAddress,
                        accountBalance: userData.accountBalance,
                        date: userData.date,
                        phoneNumber: userData.phoneNumber
                    });
                }

                // else if the route params is land market place
                else if (routeParams === "landMarketPlace") {
                    // Setting the full path to the html template file
                    let fullPath = path.join(rootPath, 'static', 'templates', 'dashboardPages', 'landMarketPlace.ejs');

                    // Rendering the land market place template file
                    return res.render(fullPath, {
                        firstname: userData.firstname,
                        lastname: userData.lastname,
                        emailAddress: userData.emailAddress,
                        accountBalance: userData.accountBalance,
                        date: userData.date,
                        phoneNumber: userData.phoneNumber
                    });

                }

                // else if the route params is resellNft
                else if (routeParams === "resellNft" ) {
                    // Setting the full path to the html template file
                    let fullPath = path.join(rootPath, 'static', 'templates', 'dashboardPages', 'resellNft.ejs');

                    // Rendering the resellNft.ejs template file
                    return res.render(fullPath, {
                        firstname: userData.firstname,
                        lastname: userData.lastname,
                        emailAddress: userData.emailAddress,
                        accountBalance: userData.accountBalance,
                        date: userData.date,
                        phoneNumber: userData.phoneNumber
                    });

                }

                // else if the route params is resellLand
                else if (routeParams === "resellLand") {
                    // Setting the full path to the html template file
                    let fullPath = path.join(rootPath, 'static', 'templates', 'dashboardPages', 'resellLand.ejs');

                    // Rendering the resellLand.ejs template file
                    return res.render(fullPath, {
                        firstname: userData.firstname,
                        lastname: userData.lastname,
                        emailAddress: userData.emailAddress,
                        accountBalance: userData.accountBalance,
                        date: userData.date,
                        phoneNumber: userData.phoneNumber
                    });
                }

                // else if the route params is deposit
                else if (routeParams === "deposit" ) {
                    // setting the full path to the html template file
                    let fullPath = path.join(rootPath, 'static', 'templates', 'dashboardPages', 'deposit.ejs');

                    // Rendering the deposit.ejs template file
                    return res.render(fullPath, {
                        firstname: userData.firstname,
                        lastname: userData.lastname,
                        emailAddress: userData.emailAddress,
                        accountBalance: userData.accountBalance,
                        date: userData.date,
                        phoneNumber: userData.phoneNumber
                    });
                }

                // else if the route params is withdraw
                else if (routeParams === "withdraw") {
                    // Setting the full path to the html template file
                    let fullPath = path.join(rootPath, 'static', 'templates', 'dashboardPages', 'withdraw.ejs');

                    // Rendering the withdraw.ejs template file
                    return res.render(fullPath, {
                        firstname: userData.firstname,
                        lastname: userData.lastname,
                        emailAddress: userData.emailAddress,
                        accountBalance: userData.accountBalance,
                        date: userData.date,
                        phoneNumber: userData.phoneNumber
                    });
                }

                // else if the route params is profile
                else if (routeParams === 'profile') {
                    // setting the full path to the html template file
                    let fullPath = path.join(rootPath, 'static', 'templates', 'dashboardPages', 'profile.ejs');

                    // Rendering the profile.ejs template file
                    return res.render(fullPath, {
                        firstname: userData.firstname,
                        lastname: userData.lastname,
                        emailAddress: userData.emailAddress,
                        accountBalance: userData.accountBalance,
                        date: userData.date,
                        phoneNumber: userData.phoneNumber,
                        countryOfResidence: userData.countryOfResidence,
                    });
                }

                // else if the route params is createNft
                else if (routeParams === 'uploadNft' ) {
                    // Setting the full path to the html template file
                    let fullPath = path.join(rootPath, 'static', 'templates', 'dashboardPages', 'uploadNft.ejs');

                    // Rendering the createNft.ejs template file
                    return res.render(fullPath, {
                        firstname: userData.firstname,
                        lastname: userData.lastname,
                        emailAddress: userData.emailAddress,
                        accountBalance: userData.accountBalance,
                        date: userData.date,
                        phoneNumber: userData.phoneNumber
                    });
                }

                // else if the route parameters is contactSupport
                else if (routeParams === "contactSupport") {
                    // setting the full path to the html template file
                    let fullPath = path.join(rootPath, 'static', 'templates', 'dashboardPages', 'contactSupport.ejs');

                    // Rendering the contactSupport.ejs template file
                    return res.render(fullPath, {
                        firstname: userData.firstname,
                        lastname: userData.lastname,
                        emailAddress: userData.emailAddress,
                        accountBalance: userData.accountBalance,
                        date: userData.date,
                        phoneNumber: userData.phoneNumber
                    })
                }


            }

            // else if the user's data is not present on the database
            else {
                // redirecting the user back to the home page
                let fullPath = path.join(rootPath, 'static', 'templates', 'home.ejs')
                return res.render(fullPath);
            }
        }

        // On error connecting to the database 
        catch(error) {
            // Error connecting to the database, redirect the user back to the home page
            let fullPath = path.join(rootPath, 'static', 'templates', 'home.ejs')
            return res.render(fullPath);
        }
    }

    else {
        // IF the user session does not exists inside the session storage, execute the code
        // below
        let fullPath = path.join(rootPath, 'static', 'templates', 'home.ejs')
        return res.render(fullPath);
    }
})


// Exporting the router object
module.exports = router;
