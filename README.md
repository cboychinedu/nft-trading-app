NFT-project

# NFT Marketplace Web Application

<img src="./static/images/readmeImage/image1.png">
<img src="./static/images/readmeImage/image2.png">
<img src="./static/images/readmeImage/image3.png">

Welcome to the NFT Marketplace web application! This application allows users to register, upload their NFT data for sale, and track the number of views their uploaded NFTs have received. It was built using Node.js, JavaScript, HTML/CSS, and EJS, with an integrated MongoDB backend for data storage.

## Table of Contents

- [Features](#features)
- [Technologies Used](#technologies-used)
- [Installation](#installation)
- [Usage](#usage)
- [Database Schema](#database-schema)
- [Contributing](#contributing)
- [License](#license)

## Features

- User Registration: Users can create an account to access the platform.
- NFT Upload: Registered users can upload their NFT data for sale.
- View Tracking: The application keeps track of the number of views each uploaded NFT has received.
- Secure Authentication: User data is securely stored and authenticated.

## Technologies Used

- **Node.js**: Backend server environment.
- **JavaScript**: Programming language for both frontend and backend logic.
- **HTML/CSS**: Frontend web development.
- **EJS**: Templating engine for generating dynamic HTML pages.
- **MongoDB**: Database for storing user data, NFT information, and view counts.

## Installation

To run this application locally, follow these steps:

1. Clone the repository:

   ```bash
   git clone <repository-url>
   ```

2. Navigate to the project directory:

   ```bash
   cd nft-project
   ```

3. Install the required dependencies:

   ```bash
   npm install .
   ```

4. Set up a MongoDB database and update the database configuration in the `config.js` file.

5. Start the server:

   ```bash
   npm start
   ```

6. Open a web browser and go to `http://localhost:3000` to access the application.

## Usage

1. Register for an account on the NFT Marketplace.

2. Log in with your credentials.

3. Upload your NFT data for sale by providing all the necessary details.

4. Your uploaded NFT will be listed on the marketplace.

5. Users can view and interact with your NFT, and the application will track the number of views.

## Database Schema

The MongoDB database for this application includes the following collections:

- **Users**: Stores user information (username, email, password).
- **NFTs**: Stores NFT details (title, description, owner, view count).
- **Views**: Stores view data for each NFT, linking it to the corresponding NFT.

## Contributing

Contributions are welcome! If you'd like to contribute to the project, please follow these steps:

1. Fork the repository.

2. Create a new branch for your feature or bug fix:

   ```bash
   git checkout -b feature-name
   ```

3. Make your changes and commit them:

   ```bash
   git commit -m "Your commit message here"
   ```

4. Push your changes to your forked repository:

   ```bash
   git push origin feature-name
   ```

5. Create a pull request to the main repository.

6. Your contribution will be reviewed, and once approved, it will be merged.

## License

This project is licensed under
