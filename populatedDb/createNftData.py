# Importing the necessary modules 
import os 
import pymongo 
import json 

# connect to the mongodv server 
client =  pymongo.MongoClient("mongodb://localhost:27017/")
db = client["nftProjects"];
collection = db["nftblobfile"]

# Getting the path to the images 
imagePath = "images";
metaDataPath = "metaData";  

# getting the full name 
images = os.listdir(imagePath)
metaDatasList = os.listdir(metaDataPath)
count = 0; 

# 
for metaData in metaDatasList: 
    fullPath = os.path.sep.join([metaDataPath, metaData])
    
    # Setting the new name 
    newName = f"img{count}.txt"; 
    newNameFullPath = os.path.sep.join([metaDataPath, newName])

    # 
    os.rename(fullPath, newNameFullPath)
    count = count + 1; 

# print(fullName); 
# 
# for image in images: 
#     fullPath = os.path.sep.join([imagePath, image])
    
#     newName = f"img{count}.jpg"; 
#     newNameFullPath = os.path.sep.join([imagePath, newName])

#     # 
#     os.rename(fullPath, newNameFullPath)
#     count = count + 1; 
# 
