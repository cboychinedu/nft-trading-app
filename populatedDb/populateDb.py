# Importing the necessary modules 
import os 
import pymongo 
import json 

# connect to the mongodv server 
client =  pymongo.MongoClient("mongodb://localhost:27017/")
db = client["nftProjects"];
collection = db["nftfiles"]

# Setting the path to the nft description 
nftMetaDataDir = "metaData"; 
nftImageDataDir = "images"; 

# Listing dir 
for index, files in enumerate(os.listdir(nftMetaDataDir)):
    # Saving the image 
    imageName = files.split(".")[0]
    imageName = f"{imageName}.jpg";

    # load the .txt file into memory
    file = os.path.sep.join([nftMetaDataDir, files])
    file = open(file, "r").readline();  

    # Creating a json object 
    nftObject = {} 
    nftObject["firstname"] = 'Mark'
    nftObject["lastname"] = 'Smith'
    nftObject["emailAddress"] = "xyz@gmail.com" 
    nftObject["nftTitle"] = "Fashion" 
    nftObject["nftArtistName"] = "Green Boar" 
    nftObject["nftCategory"] = "NFT" 
    nftObject["nftPrice"] = "0.456ETH" 
    nftObject["nftImageFileLocation"] = f"/images/{imageName}"
    nftObject["nftDescription"] = file 

    # Saving the json object 
    data = list();
    data.append(nftObject) 

    # insert the data into the mongoDb collection 
    collection.insert_many(data); 