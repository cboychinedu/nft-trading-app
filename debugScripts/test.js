// Sample array
let originalArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// Slice the array from index 0 to 4
let slicedArray = originalArray.slice(0, 4);

// Remove the values from the original array using splice()
originalArray.splice(0, 4);

console.log(originalArray); 