#!/usr/env python3 

# Importing the necessary modules 
import os 
import time 
import subprocess 

# Creating the main function
def main():
    # Using a while loop 
    while True:
        print("[INFO] Executing the shell script...")
        # Executing the shell script 
        result = subprocess.run("sh deploy.sh", 
                                shell=True, stdout=subprocess.PIPE, 
                                stderr=subprocess.PIPE, text=True)
        
        # Getting the result 
        result = str(result.stdout) 
        gitMessage = result.split("\n")[-2] 

        # Using if else statement if the gitMessage is up to date.
        if (gitMessage == 'Already up to date.'): 
            # Wait for 5 second 
            time.sleep(5)

            # Skip and start the while loop again  
            continue; 
        
        # Else, execute the block of code below 
        else:
            # If the push commit was made, execute the block of code below
            print("[INFO] Restarting the process...")
            result = subprocess.run("pm2 restart app.js",
                                    shell=True, stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE, text=True)
            # Wait for 5 second 
            time.sleep(5)


       
# Running the application 
if __name__ == "__main__":
    main()