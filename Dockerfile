# Use Ubuntu as the base image 
FROM ubuntu

# Set the working directory inside the container
WORKDIR /app 

# Install nodejs and pip 
RUN apt-get update && apt-get install nodejs npm -y 

# Copy the package.json file 
COPY package.json . 

# Install all dependencies 
RUN npm install . 
RUN npm install pm2 nodemon -g 

# Copy the current directory contents into the container at /app 
COPY . . 

# Expose port 5000 to the host 
EXPOSE 5000:5000

# Run the command 
CMD ["pm2", "start", "app.js"]