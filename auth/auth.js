// Importing the necessary modules 
require('dotenv').config(); 
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcrypt'); 

// Checking the token data for verification by getting the password 
const tokenPassword = process.env.tokenPassword || "token_password"; 

// Creating a middle ware function to check if the user is logged in 
const protectedRoute = (req, res, next) => {
    // Getting the value for the user's authenticaton, and if it's valid 
    try {
        // Checking the is auth value 
        let isAuth = req.session.isAuth; 

        // Checking if the user is a valid user 
        if (isAuth) {
            // Move on to the next middleware 
            next(); 
        }

        // If the user is not authenticated 
        else {
            // Redirect the user back to the login page 
            return res.redirect('/'); 
        }
    }

    // Catch the error 
    catch (error) {
        // Redirect the user back to the login page 
        return res.redirect('/'); 
    }
}

// Creating a middleware function for registering a new admin 
const registerNewAdmin = async (req, res, next) => {
    // Getting the admin register password 
    const registerPassword = req.body.registerPassword; 

    // Verifying 
    try {
        // Verifying, by getting the server password 
        const serverPassword = process.env.serverPasswordHash; 
        let passwordCondition = await bcrypt.compare(registerPassword, serverPassword); 

        // If the password Condition is true 
        if (passwordCondition) {
            // Move on to the next request handler 
            next(); 
        }

        // Else if the user password for the admin was wrong, 
        else {
            // Execute the block of code below 
            let successMessage = JSON.stringify({
                "message": "Create Admin password not correct", 
                "status": "error", 
                "statusCode": 500, 
            })

            // Sending back the success message 
            return res.send(successMessage).status(successMessage["statusCode"]); 

        }
    }
    // if there was an error connecting to the server, execute the block of code below 
    catch(error) {
        // Create an error message with a status code 
        let successMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error", 
            "statusCode": 400, 
        })

        // Sending back the success message 
        return res.send(successMessage).status(successMessage["statusCode"]); 

    }
}

// Exporting the auth module
module.exports.protectedRoute = protectedRoute;
module.exports.registerNewAdmin = registerNewAdmin; 