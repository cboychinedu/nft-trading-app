// Debug 
console.log("Debug")

// Getting all the dom elements for the register input form 
const firstname = document.getElementById("firstname"); 
const lastname = document.getElementById("lastname"); 
const phoneNumber = document.getElementById("phoneNumber"); 
const email = document.getElementById("email"); 
const password = document.getElementById("password"); 
const confirmPasswordValue = document.getElementById("confirmPassword"); 
const registerButton = document.getElementById("registerButton"); 
const loaderDiv = document.getElementById("loaderDiv");
const mainNavBarDiv = document.getElementById("mainNavbarDiv");  


// Show the loader div when the page loads 
// document.addEventListener("DOMContentLoaded", () => {

//     mainNavBarDiv.style.display = "none";
//     loaderDiv.style.display = "flex";  

//     // Setting time out  
//     setTimeout(hideLoaderDiv, 1000); 
// })

// Creating a function for hiding the loader 
// let hideLoaderDiv = () => {
//     // Hiding the loader div 
//     loaderDiv.style.display = "none"; 
//     mainNavBarDiv.style.display = "flex"; 
// }

// Adding the event listener for the register button 
registerButton.addEventListener("click", (event) => {
    // Preventing default submission 
    event.preventDefault(); 

    // Checking if the firstname field was filled 
    if (!firstname.value) {
        Swal.fire({
            title: "Firstname is required...",  
            icon: "error", 
            html: "<p class='swalPara'> Firstname is missing, <br> please type in your firstname... </p>", 
            confirmPassword: "Okay...", 
        })
    }

    // Checking if the lastname name field was filled 
    else if (!lastname.value) {
        Swal.fire({
            title: "Lastname is required...",  
            icon: "error", 
            html: "<p class='swalPara'> Lastname is missing,  <br> please type in your lastname... </p>", 
            confirmPassword: "Okay...", 
        })
    }

    // Checking if the phone number was filled 
    else if (!phoneNumber.value) {
        Swal.fire({
            title: "Phonenumber is required...",  
            icon: "error", 
            html: "<p class='swalPara'> Phonenumber is missing,  <br> please type in your phonenumber... </p>", 
            confirmPassword: "Okay...", 
        })
    }

    // Checking if the email address was filled 
    else if (!email.value) {
        Swal.fire({
            title: "Email is required...",  
            icon: "error", 
            html: "<p class='swalPara'> Email is missing,  <br> please type in your email... </p>", 
            confirmPassword: "Okay...", 
        })
    }

    // Checking if the password address was filled 
    else if (!password.value) {
        Swal.fire({
            title: "Password is required...",  
            icon: "error", 
            html: "<p class='swalPara'> Password is missing, <br> please type in your password... </p>", 
            confirmPassword: "Okay...", 
        }) 
    }

    // Checking if the password is the same and confirmed 
    else if (!confirmPasswordValue.value) {
        Swal.fire({
            title: "Confirm password field is required...", 
            icon: "error", 
            html: "<p class='swalPara'> Confirm password is missing, <br> please type in your password... </p>", 
            confirmPassword: "Okay...", 

        })
    }

    // Else 
    else if (password.value != confirmPasswordValue.value) {
        Swal.fire({
            title: "The passwords do not match", 
            icon: "error", 
            html: "<p class='swalPara'> Passwords mismatch, <br> please retype the passwords again... </p>", 
            confirmPassword: "Okay...", 

        })
    }

    // Else 
    else { 

        // Getting all the user's data 
        let usersData = JSON.stringify({
            "firstname": firstname.value, 
            "lastname": lastname.value, 
            "phoneNumber": phoneNumber.value, 
            "emailAddress": email.value, 
            "password": password.value
        })

        // Sending the data to the backend server 
        fetch("/register", {
            method: "POST", 
            headers: {
                'Content-Type': 'application/json', 
            }, 
            body: usersData
        }).then(response => response.json())
        .then(data => {
            // Handle the response data 
            if (data.status === "success") { 

                // Redirect to another page after 2 seconds
                setTimeout(() => {
                    window.location.href = '/postRegister';
                }, 3000);
            }

            // Else if there was an error in registeration 
            else if (data.status === "error") {

                // Redirect to another page after 2 seconds
                setTimeout(() => {
                    // Using swal alert to display the error message 
                    Swal.fire({
                        title: "Error in registration", 
                        icon: "error", 
                        html: `<p class='swalPara'> ${data.message} </p>`, 
                        confirmPassword: "Okay...", 
                    })
                }, 3000);
            }
        })
        .catch(error => {
            // handle any error that occurred the request 
            console.error('Error: ',  error); 
        })
    }

})