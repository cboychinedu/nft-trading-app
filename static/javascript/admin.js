// Debug 
console.log('Working')

// Getting all the dom elements 
const email = document.getElementById("emailAddress"); 
const password = document.getElementById("password"); 
const loginBtn = document.getElementById("loginBtn"); 

// Adding event listener for the login button 
loginBtn.addEventListener("click", (event) => {
    // Checking if the email field was not filled 
    if (!email.value) {
        Swal.fire({
            title: "Email field is required...", 
            text: "Email address is missing", 
            icon: "error", 
            confirmButtonText: "Okay...", 
        })
    }

    // Checking if the password value is filled 
    else if (!password.value) {
        Swal.fire({
            title: "Password is required...", 
            text: "Password is required...", 
            icon: "error", 
            confirmButtonText: "Okay...", 
        })
    }

    // Else 
    else {
        // else if, getting all the data values 
        data = JSON.stringify({
            emailAddress: email.value, 
            password: password.value
        })

        // Making a fetch request to the admin login server 
        fetch('/admin/login', {
            method: 'POST', 
            body: data, 
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .then(response => response.json())
        .then(response => {
            // Getting the response for error message 
            if (response.status === "error") {
                Swal.fire({
                    title: "Invalid username or password", 
                    text: "Invalid username or password", 
                    icon: "error", 
                    confirmButtonText: "Okay...", 
                })
            }

            // if else 
            else if  (response.status === 'success') {
                window.location.href = '/admin/dashboard'; 
            }
           
        })
    }
})