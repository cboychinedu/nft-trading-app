// Creating a list to hold the cart items 
let cartItems = []; 

// Getting the dom element 
const cartBottomDiv = document.getElementById("cartBottomDiv"); 
const searchInputForm = document.getElementById("searchInputForm"); 
const searchBtn = document.getElementById("searchBtn"); 

// Adding event listener for the search btn 
searchBtn.addEventListener("click", (event) => {
  // Getting the data from the search input form 
  const searchData = searchInputForm.value; 

  // Setting the data 
  const data = JSON.stringify({
     "searchData": searchData, 
  })

  // Sending the data to the nftSearch route 
  fetch('/nftData/searchNft', {
    method: 'POST', 
    headers: {
      'Content-Type': 'application/json'
    }, 
    body: data, 
  })
  .then((response) => response.json())
  .then((responseData) => { 

    // Get the grid container
    const gridContainer = document.getElementById('imageGrid');
    gridContainer.innerHTML = ""; 


    // Loop through the responseData and create grid items for each items 
    // found in the responseData 
    responseData.forEach(data => {
      // console.log(data); 

      // Creating a div called gridItem 
      const gridItem = document.createElement('div'); 
      gridItem.classList.add('gridItem'); 

      // Adding the nft Image div
      const nftImageDiv = document.createElement("div"); 
      
      // Getting the image name 
      let imageName = data.nftImageFileLocation; 
      imageName = imageName.split('/uploads/')[1]; 
      
      nftImageDiv.className = "nftImageDiv"; 
      nftImageDiv.innerHTML = `
        <img src="/images/marketplace/${imageName}" class="nftDataImage" alt=""/> 
      `

      // Appending the nftImageDiv into the gridItem 
      gridItem.appendChild(nftImageDiv); 

      // Adding the views div 
      const viewsDiv = document.createElement("div"); 
      viewsDiv.className = "views"; 
      viewsDiv.innerHTML = `
        <div class="viewsLogoDiv">
        <img src="../../images/dashboard/nftMarketPlace/views.png" class="viewsLogo" alt="">
        </div>
        <div class="viewsCountDiv">
            <p class="viewsCountPara"> 3.4K views </p>
        </div>
      `

      // Appending the views Div into the gridItem 
      gridItem.appendChild(viewsDiv); 

      // Adding the username div 
      const userProfileDiv = document.createElement("div"); 
      userProfileDiv.className = "userProfileDiv"; 
      userProfileDiv.innerHTML = `
        <div> 
          <img class="profileImage" src="../../images/profile/Logo.png" /> 
        </div> 
        <div class="userDetailsDiv">
          <p class="userDetails"> ${data.nftArtistName} </p> 
          <p class="userDetails"> ${data.nftTitle} </p>  
        </div> 
      
      `

      // Appending the userProfile Div 
      gridItem.appendChild(userProfileDiv); 

      // Adding the buy button 
      const buyBtnDiv = document.createElement("div"); 
      buyBtnDiv.className = "buyBtnDiv"; 

      const buyBtn = document.createElement("button"); 
      buyBtn.className = "buyBtn"
      buyBtn.innerHTML = `
          Buy <span class="priceTag"> ${data.nftPrice} </span> ETH 
      `

      // Adding event listener to the buy button
      buyBtn.addEventListener("click", (event) => {
        // Execute the block of code below if the buy button was clicked 
        // Converting the data into a json object 
        // Saving the data to the cart items list 
        cartItems.push(data); 

        // Getting the cart number value 
        const cartNumberValue = document.getElementById("cartNumberValue");
        cartNumberValue.innerText = cartItems.length;  



        // Redirect the user to the payment page with the nft selected as a 
        // url :/parameter 
      })

      // 
      buyBtnDiv.appendChild(buyBtn); 
      gridItem.appendChild(buyBtnDiv); 
      

      // Appending the grid Item into the gridContainer 
      gridContainer.appendChild(gridItem);
      


    })
  })
  

})

// Adding event listener 
cartBottomDiv.addEventListener("click", (event) => {
  // Setting the data 
  const data = JSON.stringify({
    "data": cartItems, 
  })
    // Sending the Checked data to the cart route 
    fetch('/dashboard/addCart', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }, 
      body: data, 
    })
    .then((response) => response.json())
    .then((responseData) => {
      // Getting the cart _id value 
      const cartId = responseData._id; 

      // Redirecting the user to the View Cart page 
      window.location.href = `/dashboard/viewCart/${cartId}`; 
      
    }) 
})

  // Using fetch request to get all the nft data 
fetch('/nftData', {
  method: 'GET', 
  headers: { 'Content-Type': 'application/json'}, 
  body: null, 
})
.then(response => response.json())
.then(responseData => {
  // Getting the nft data from the database 
  // console.log(responseData); 

  // Get the grid container
  const gridContainer = document.getElementById('imageGrid');

  // Loop through the responseData and create grid items for each items 
  // found in the responseData 
  responseData.forEach(data => {
    // console.log(data); 

    // Creating a div called gridItem 
    const gridItem = document.createElement('div'); 
    gridItem.classList.add('gridItem'); 

    // Adding the nft Image div
    const nftImageDiv = document.createElement("div"); 
    
    // Getting the image name 
    let imageName = data.nftImageFileLocation; 
    imageName = imageName.split('/uploads/')[1]; 
    
    nftImageDiv.className = "nftImageDiv"; 
    nftImageDiv.innerHTML = `
      <img src="/images/marketplace/${imageName}" class="nftDataImage" alt=""/> 
    `

    // Appending the nftImageDiv into the gridItem 
    gridItem.appendChild(nftImageDiv); 

    // Adding the views div 
    const viewsDiv = document.createElement("div"); 
    viewsDiv.className = "views"; 
    viewsDiv.innerHTML = `
      <div class="viewsLogoDiv">
      <img src="../../images/dashboard/nftMarketPlace/views.png" class="viewsLogo" alt="">
      </div>
      <div class="viewsCountDiv">
          <p class="viewsCountPara"> 3.4K views </p>
      </div>
    `

    // Appending the views Div into the gridItem 
    gridItem.appendChild(viewsDiv); 

    // Adding the username div 
    const userProfileDiv = document.createElement("div"); 
    userProfileDiv.className = "userProfileDiv"; 
    userProfileDiv.innerHTML = `
      <div> 
        <img class="profileImage" src="../../images/profile/Logo.png" /> 
      </div> 
      <div class="userDetailsDiv">
        <p class="userDetails"> ${data.nftArtistName} </p> 
        <p class="userDetails"> ${data.nftTitle} </p>  
      </div> 
    
    `

    // Appending the userProfile Div 
    gridItem.appendChild(userProfileDiv); 

    // Adding the buy button 
    const buyBtnDiv = document.createElement("div"); 
    buyBtnDiv.className = "buyBtnDiv"; 

    const buyBtn = document.createElement("button"); 
    buyBtn.className = "buyBtn"
    buyBtn.innerHTML = `
        Buy <span class="priceTag"> ${data.nftPrice} </span> ETH 
    `

    // Adding event listener to the buy button
    buyBtn.addEventListener("click", (event) => {
      // Execute the block of code below if the buy button was clicked 
      // Converting the data into a json object 
      // Saving the data to the cart items list 
      cartItems.push(data); 

      // Getting the cart number value 
      const cartNumberValue = document.getElementById("cartNumberValue");
      cartNumberValue.innerText = cartItems.length;  



      // Redirect the user to the payment page with the nft selected as a 
      // url :/parameter 
    })

    // 
    buyBtnDiv.appendChild(buyBtn); 
    gridItem.appendChild(buyBtnDiv); 
    

    // Appending the grid Item into the gridContainer 
    gridContainer.appendChild(gridItem);
    


  })
})

