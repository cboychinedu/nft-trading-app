// On document load 
console.log("Working")

// Getting the id Cart Value 
const cartIdValue = window.location.pathname.split("/viewCart/")[1]; 
const upperCartDiv = document.getElementById("upperCartDiv"); 

// Getting the totals html tags 
const subTotal = document.getElementById("subTotal"); 
const discount = document.getElementById("discount"); 
const total = document.getElementById("total"); 

// Getting the checkout button dom element 
const checkoutBtn = document.getElementById("checkoutBtn"); 


// Setting the data for the cartId 
const data = JSON.stringify({
    "cartId": cartIdValue
})

// Making a post request to calculate the total price for each 
// selected cart 
fetch('/dashboard/calculateCartPrice', {
    method: 'POST', 
    headers: { 'Content-Type': 'application/json'}, 
    body: data, 
})
.then((response) => response.json())
.then((responseData) => {
    // Getting the cart total 
    const cartSubTotal = responseData.cartSubTotal; 
    const cartDiscount = responseData.cartDiscount; 
    const cartTotal = responseData.cartTotal; 

    // Making changes to the html paragraphs 
    subTotal.innerText = cartSubTotal; 
    discount.innerText = cartDiscount; 
    total.innerText = cartTotal; 

})

// Making a post request to the /viewCart/:routeParams server 
fetch(`/dashboard/viewCart/${cartIdValue}`, {
    method: 'POST', 
    headers: {
        'Content-Type': 'application/json'
    }, 
    body: null, 
})
.then((response) => response.json())
.then((responseData) => {
    // Changing the response Data 
    responseData = responseData.items; 

    // Checking if the responseData is more than 2 
    if (responseData.length > 2) {
        // 
        upperCartDiv.style.height = "400px"; 
    }

    // 
    else if (responseData.length < 2) {
        // 
        upperCartDiv.style.height = "fit-content"; 
    }

    // Looping throug the array 
    responseData.forEach((items) => {
        // Creating the innerUpperCart Div 
        const innerUpperCartDiv = document.createElement("div"); 
        innerUpperCartDiv.className = "innerUpperCartDiv"; 

        // Getting the image name 
        let imageName = items.nftImageFileLocation; 
        imageName = imageName.split("/uploads/")[1]; 

        // Adding the data 
        innerUpperCartDiv.innerHTML = `
            <div> 
                <img src="/images/marketplace/${imageName}" class="nftImage" /> 
            </div>

            <div class="innerDescriptionDiv">
                <div>
                    <p class="descriptionPara"> ${items.nftDescription} </p>
                </div>

                <div class="priceDiv">
                    <p class="fullname"> 
                        ${items.firstname} ${items.lastname}
                    </p> 
                    <p class="price"> 
                        <b> ETH ${items.nftPrice} </b>
                    </p>
                </div>
            </div>
        `; 

        // Adding the innerUpperCartDiv into the upperCartDiv 
        upperCartDiv.appendChild(innerUpperCartDiv); 
        
    })
    
})