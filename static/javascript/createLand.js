// Debug 
console.log("working")

// Getting all the dom elements 
let landNftTitle = document.getElementById("landNFtTitle"); 
let landNftArtistName = document.getElementById("landNftArtistName"); 
let landCategory = document.getElementById("category"); 
let landNftPrice = document.getElementById("landNftPrice"); 
let landNftDescription = document.getElementById("landNftDescription"); 
let landNftFileData = document.getElementById("landFileData"); 
let createLandButton = document.getElementById("landNftCreateButton"); 

// Adding event listener for the create land button 
createLandButton.addEventListener("click", (e) => {
    // Preventing default submission 
    e.preventDefault(); 

    // Checking if the land nft title was filled 
    if (!landNftTitle.value) {
        Swal.fire({
            title: "Land Nft title is required...",
            icon: "error",
            html: "<p class='swalPara'> Land Ntf title is missing, <br> please fill the form... </p>",
            confirmPassword: "Okay...",
        })
        return; 
    }
    
    // Checking if the landNftArtistName was filled 
    else if (!landNftArtistName.value) {
        Swal.fire({
            title: "Land Nft artist is required...",
            icon: "error",
            html: "<p class='swalPara'> Land Ntf artist is missing, <br> please fill the form... </p>",
            confirmPassword: "Okay...",
        })
        return; 
    }

    // Checking if the landNftPrice was filled 
    else if (!landNftPrice.value) {
        Swal.fire({
            title: "Nft land price is required...",
            icon: "error",
            html: "<p class='swalPara'> Nft land price is missing, <br> please fill the form...</p>",
            confirmPassword: "Okay..."
        })
        return; 
    }

    // Checking if the landNftDescription was filled 
    else if (!landNftDescription.value) {
        Swal.fire({
            title: "Land Nft description is required...",
            icon: "error",
            html: "<p class='swalPara'> Land Nft description is missing, <br> please fill the form... </p>",
            confirmPassword: "Okay..."
        })
        return; 
    }

    // Checking if the landNftFileData was filled 
    else if (!landNftFileData.value) {
        Swal.fire({
            title: "Land Nft File missing...",
            icon: "error",
            html: "<p class='swalPara'> Land Nft file missing </p> ",
            confirmPassword: "Okay...",
        })
        return; 
    }

    // Else 
    else {
        // Sendin the form data using fetch request 
        // Making the form data 
        const formData = new FormData(); 
        formData.append('myFile', landNftFileData.files[0]); 
        formData.append('landNftTitle', landNftTitle.value); 
        formData.append('landNftArtistName', landNftArtistName.value); 
        formData.append("landNftCategory", landCategory.value); 
        formData.append("landNftPrice", landNftPrice.value); 
        formData.append("landNftDescription", landNftDescription.value); 

        // Send the form data using fetch request 
        fetch('http://localhost:5000/dashboard/createLand', {
            method: 'POST', 
            body: formData
        })
        .then((response) => response.json())
        .then(data => {
             // Execute the block of code below if the form was submitted successfully 
        if (data.status === "success") {
            Swal.fire({
                title: "Nft Uploaded...",
                icon: "success",
                html: "<p class='swalPara'> Land Nft data uploaded </p> ",
                confirmPassword: "Okay...",
            })
            .then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                  // If the result is confirmed 
                  setTimeout(() => {
                    window.location.reload(); 
                }, 2000);
                } 
              })
        }

          // Else if the data uploaded returned an error message 
          else if (data.status === "error") {
            Swal.fire({
                title: "Error in upload...", 
                icon: "error", 
                html: "<p class='swalPara'> Error occured while uploading the nft data </p>", 
                confirmPassword: "Okay...", 
            })
        }

        })
    }
})