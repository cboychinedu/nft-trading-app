let header = document.getElementById("mainNavbarDiv");

// Creating a function for generating random number 
let generateRandomNumber = () => {
  // Setting the minimum number 
  const minNumber = 10000; 

  // Setting the maximum number 
  const maxNumber = 99999; 

  // Generating the random number 
  const result = Math.floor(Math.random() * (maxNumber - minNumber + 1)) + minNumber; 

  // Saving the number inside the html tag 
  randomNumber.innerText = result; 

  // Return the number 
  return result; 
}

// Get the offset position of the navbar
let sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scrool position. Remove
// "sticky" when you leave the scroll position
function staticScroll()
{
    // Code execution
    if ( window.pageYOffset > sticky )
    {
        // if the condition is met, execute the code below
        header.classList.add("sticky");
    }

    // Else condition
    else
    {
        // code execution
        header.classList.remove("sticky");
    }
};

// Making the navigator tag fixed on scrolling
window.onscroll = function() {
  staticScroll()
};

var menuButton = document.getElementById('menuButton');
var menuContainer = document.getElementById('menuContainer');


menuButton.addEventListener('click', function() {
    menuContainer.classList.add('open');
  
    // Remove the menu after it is shown
    setTimeout(function() {
        menuContainer.classList.remove('open');
    }, 3000); // Adjust the time in milliseconds (e.g., 3000 = 3 seconds)
});