// Getting the dom elements 
const slider = document.getElementById('slider');
const images = slider.getElementsByTagName('section');
const dots = document.getElementsByClassName("dot")
const bottomDiv = document.getElementById('bottomDiv');
const chatLogo = document.getElementById("chatLogo"); 
const chatDiv = document.getElementById("chatDiv");
const chatCloseLogo = document.getElementById("chatCloseLogo"); 
const loaderWrapper = document.querySelector('.loaderDiv');

let currentImage = 0;

// Adding event listener for the load 
window.addEventListener('load', () => {
    // Wait first for 3 seconds 
    setTimeout((e) => {
        // Remove the loader 
        loaderWrapper.style.display = 'none';
    }, 2000)
    
});

// Adding event listener for the chat logo 
chatLogo.addEventListener("click", (event) => {
    bottomDiv.style.display = "none"; 
    chatDiv.classList.add('open'); 

})

// Adding event listener for the chat close logo 
chatCloseLogo.addEventListener("click", (event) => {
    bottomDiv.style.display = "block"; 
    chatDiv.classList.remove("open"); 
})


function showImage(imageIndex) {
    for (let i = 0; i < images.length; i++) {
        images[i].style.display = 'none';
        dots[i].classList.remove('active');
    }
    images[imageIndex].style.display = 'block';
    dots[imageIndex].classList.add('active');
}

// Initialize the display
showImage(currentImage);

// Change the image when clicking on a dot
for (let i = 0; i < dots.length; i++) {
    dots[i].addEventListener('click', function() {
        currentImage = i;
        showImage(currentImage);
    });
}

// Automatically change the image every 2 seconds
setInterval(function() {
    currentImage = (currentImage + 1) % images.length;
    showImage(currentImage);
}, 2000);



// Get the ball element
// var ball = document.getElementById('ball');

// // Track mouse movement
// document.addEventListener('mousemove', function(event) {
//     // Get the current mouse coordinates
//     var mouseX = event.clientX;
//     var mouseY = event.clientY;

//     // Delay the ball position update by 3 seconds
//     setTimeout(function() {
//         // Update the position of the ball
//         ball.style.left = (mouseX - 15) + 'px';
//         ball.style.top = (mouseY - 15) + 'px';
//     }, 100);
// });


// var screenWidth = window.outerWidth
// var screenHeight = window.outerHeight

// alert(screenHeight)

// // Get the ball and cursor elements
// var ball = document.getElementById('ball');
// var cursor = document.getElementById('cursor');

// // Track mouse movement
// document.addEventListener('mousemove', function(event) {
//     // Get the current mouse coordinates
//     var mouseX = event.clientX;
//     var mouseY = event.clientY;

//     // Update the position of the ball
//     ball.style.left = (mouseX - 15) + 'px';
//     ball.style.top = (mouseY - 15) + 'px';

//     // Update the position of the cursor
//     cursor.style.left = (mouseX - 10) + 'px';
//     cursor.style.top = (mouseY - 10) + 'px';
// });