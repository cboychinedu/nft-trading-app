// Debug 
console.log("Working"); 

// Getting all the dom element 
const loaderDiv = document.getElementById("loaderDiv");
const mainNavBarDiv = document.getElementById("mainNavbarDiv"); 
const slider = document.getElementById('slider');
const images = slider.getElementsByTagName('section');
const dots = document.getElementsByClassName("dot")
let currentImage = 0;;



function showImage(imageIndex) {
    for (let i = 0; i < images.length; i++) {
        images[i].style.display = 'none';
        dots[i].classList.remove('active');
    }
    images[imageIndex].style.display = 'block';
    dots[imageIndex].classList.add('active');
}

// Initialize the display
showImage(currentImage);

// Change the image when clicking on a dot
for (let i = 0; i < dots.length; i++) {
    dots[i].addEventListener('click', function() {
        currentImage = i;
        showImage(currentImage);
    });
}

// Automatically change the image every 2 seconds
setInterval(function() {
    currentImage = (currentImage + 1) % images.length;
    showImage(currentImage);
}, 2000); 

// Show the loader div when the page loads 
// document.addEventListener("DOMContentLoaded", () => {

//     mainNavBarDiv.style.display = "none";
//     loaderDiv.style.display = "flex";  

//     // Setting time out  
//     setTimeout(hideLoaderDiv, 1500); 
// })

// // Creating a function for hiding the loader 
// let hideLoaderDiv = () => {
//     // Hiding the loader div 
//     loaderDiv.style.display = "none"; 
//     mainNavBarDiv.style.display = "flex"; 
// }


