
// Array of image URLs
const imageList = [
    'image1.jpg',
    'image2.jpg',
    'image3.jpg',
    'image4.jpg',
    'image5.jpg',
    'image1.jpg',
    'image2.jpg',
    'image3.jpg',
    'image4.jpg',
    'image5.jpg',
    'image1.jpg',
    'image2.jpg',
  ];
  
  // Get the grid container
  const gridContainer = document.getElementById('imageGrid');
  
  // Loop through the imageList and create grid items for each image
  imageList.forEach(imageUrl => {
      // Creating a div called gridItem 
      const gridItem = document.createElement('div');
      gridItem.classList.add('gridItem');
  
      // Adding the nft Image div 
      const nftImageDiv = document.createElement("div"); 
      nftImageDiv.className = "nftImageDiv"; 
      nftImageDiv.innerHTML = `
          <img src="../../images/dashboard/nftMarketPlace/digitalArt.jpg" class="nftDataImage" alt="">
      `
      gridItem.appendChild(nftImageDiv); 
  
      // Adding the views div 
      const viewsDiv = document.createElement("div"); 
      viewsDiv.className = "views"; 
      viewsDiv.innerHTML = `
        <div class="viewsLogoDiv">
          <img src="../../images/dashboard/nftMarketPlace/views.png" class="viewsLogo" alt="">
        </div>
        <div class="viewsCountDiv">
            <p class="viewsCountPara"> 1.4M views </p>
        </div>
  
      `
      gridItem.appendChild(viewsDiv); 
  
  
      // Adding the username div 
      const userProfileDiv = document.createElement("div"); 
      userProfileDiv.className = "userProfileDiv"; 
      userProfileDiv.innerHTML = `
        <div>
          <img class="profileImage" src="../../images/dashboard/nftMarketPlace/jjjacobs.jpg" alt="" srcset="">
        </div>
  
        <div class="userDetailsDiv">
            <p class="userDetails"> Violet Eyes </p>
            <p class="userDetails"> John Stevens </p>
        </div>
      
      `
      gridItem.appendChild(userProfileDiv); 
  
  
      // Adding the buy button 
      const buyBtnDiv = document.createElement("div"); 
      buyBtnDiv.className = "buyBtnDiv"; 
  
      const buyBtn = document.createElement("button"); 
      buyBtn.className = "buyBtn"
      buyBtn.innerHTML = `
          Buy <span class="priceTag"> 0.45 </span> ETH 
      `
  
      // 
      buyBtn.addEventListener("click", (event) => {
         alert("NFT Bought")
  
         // Redirect the user to the payment page with the nft selected as a 
         // url :/parameter 
      })
  
      // 
      buyBtnDiv.appendChild(buyBtn); 
      gridItem.appendChild(buyBtnDiv); 
      
  
      // Appending the grid Item into the gridContainer 
      gridContainer.appendChild(gridItem);
  });
  