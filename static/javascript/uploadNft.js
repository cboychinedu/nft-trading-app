// Debug
console.log("Create nft");

// Getting all the dom elements
let nftTitle = document.getElementById("nftTitle");
let nftArtistName = document.getElementById("nftArtistName");
let nftCategory = document.getElementById("category");
let nftPrice = document.getElementById("nftPrice");
let nftDescription = document.getElementById("nftDescription");
let nftUploadBtn = document.getElementById("nftBlobFile");
let nftCreateButton = document.getElementById("createNftButton");
let fileUpload = document.getElementById("fileid"); 
let fileName = document.getElementById("uploadedFileName"); 

// Adding event listener for the fileUpload 
fileUpload.addEventListener("change", (event) => {
    // Getting the file name 
    const uploadedFilename = fileUpload.files[0].name;
    
    // Displaying the file name inside the fileName para, to 
    // show the user that the file has been uploaded into the 
    // computer memory, which is then waiting for the final upload 
    // to the backend server 
    fileName.innerText = uploadedFilename; 
})


// Adding event listener for the nftUploadBtn 
nftUploadBtn.addEventListener("click", (event) => {
    // Clicking the fileUpload button 
    fileUpload.click(); 
})

// Creating an event listener for the create nft button
nftCreateButton.addEventListener("click", (e) => {
    e.preventDefault(); 
    
    // Checking if the ntf title was not filled
    if (!nftTitle.value) {
        Swal.fire({
            title: "Nft title is required...",
            icon: "error",
            html: "<p class='swalPara'> Ntf title is missing, <br> please fill the form... </p>",
            confirmPassword: "Okay...",
        })
        return; 
    }

    // Else if the nft artistname was not filled
    else if (!nftArtistName.value) {
        Swal.fire({
            title: "Nft artist name is required...",
            icon: "error",
            html: "<p class='swalPara'> Nft artist name is missing, <br> please fill the form...</p>",
            confirmPassword: "Okay...",
        })
        return; 
    }

    // Else if the nftPrice was not filled
    else if (!nftPrice.value) {
        Swal.fire({
            title: "Nft price is required...",
            icon: "error",
            html: "<p class='swalPara'> Nft price is missing, <br> please fill the form...</p>",
            confirmPassword: "Okay..."
        })
        return; 
    }

    // else if the nftDescription was not filled
    else if (!nftDescription.value) {
        Swal.fire({
            title: "Nft description is required...",
            icon: "error",
            html: "<p class='swalPara'> Nft description is missing, <br> please fill the form... </p>",
            confirmPassword: "Okay..."
        })
        return; 
    }

    // Else if the file was not uploaded, execute the block of code below
    else if (!fileUpload.value) {
        Swal.fire({
            title: "Nft File missing...",
            icon: "error",
            html: "<p class='swalPara'> Nft file missing </p> ",
            confirmPassword: "Okay...",
        })
        return; 
    }

    // Making fetch request to the backend server to save the nft data
    const formData = new FormData(); 
    formData.append("myFile", fileUpload.files[0]); 
    formData.append("nftTitle", nftTitle.value); 
    formData.append("nftArtistName", nftArtistName.value); 
    formData.append("nftCategory", nftCategory.value); 
    formData.append("nftPrice", nftPrice.value); 
    formData.append("nftDescription", nftDescription.value);     

    // Send the form data using Fetch
    fetch('/dashboard/createNft', {
        method: 'POST',
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        // Execute the block of code below if the form was submitted successfully 
        if (data.status === "success") {
            Swal.fire({
                title: "Nft Uploaded...",
                icon: "success",
                html: "<p class='swalPara'> Nft data uploaded </p> ",
                confirmPassword: "Okay...",
            })
            .then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                  // If the result is confirmed 
                  setTimeout(() => {
                    // Reload the screen 
                    window.location.reload(); 
                }, 500);
                } 
              })
        }

        // Else if the data uploaded returned an error message 
        else if (data.status === "error") {
            Swal.fire({
                title: "Error in upload...", 
                icon: "error", 
                html: "<p class='swalPara'> Error occured while uploading the nft data </p>", 
                confirmPassword: "Okay...", 
            })
        }

    // Handle the response data as needed
    })
    .catch(error => {
        console.error('Error submitting form:', error);
    // Handle any errors that occur during the form submission
    });

    // // Sending the data to the backend server
    // fetch("http://localhost:5000/dashboard/createNft", {
    //     method: "POST"
    //     body: nftDataCollections,
    // })
    // .then(response => response.json())
    // .then((data) => {
    //     console.log(data);
    // })

    // console.log(nftFileData.files[0]);

})

// 
// uploadNftForm.addEventListener("click", (e) => {
//     e.preventDefault(); 

//     const formData = new FormData(); 
//     formData.append("myFile", nftFileData.files[0]); 


//     // Send the form data using Fetch
//     fetch('http://localhost:5000/dashboard/createNft', {
//     method: 'POST',
//     body: formData
//     })
//     .then(response => response.json())
//     .then(data => {
//     console.log('Form submitted successfully!', data);
//     // Handle the response data as needed
//     })
//     .catch(error => {
//     console.error('Error submitting form:', error);
//     // Handle any errors that occur during the form submission
//     });
// })
