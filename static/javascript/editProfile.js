// // Debug 
// console.log("Working")

// Getting all the dom element 
const modal = document.getElementById("modal"); 
const modalEditProfile = document.getElementById("modalEditProfile"); 
const changePasswordModalBtn = document.getElementById("changePasswordModalBtn"); 
const changePasswordBtn = document.getElementById("changePasswordBtn");
const editProfileBtn = document.getElementById("editProfileBtn") 
const closeModalBtn = document.getElementById("closeModalBtn"); 
const closeEditProfileBtn = document.getElementById("closeEditProfileBtn"); 
const oldPassword = document.getElementById("oldPassword"); 
const newPassword = document.getElementById("newPassword"); 


// Getting the dom element for the edit profile 
const firstname = document.getElementById("firstname"); 
const lastname = document.getElementById("lastname"); 
const phoneNumber = document.getElementById("phonenumber"); 
const emailAddress = document.getElementById("emailAddress"); 
const countryOfResidence = document.getElementById("countryOfResidence"); 
const submitBtn = document.getElementById("submitBtn"); 
const contactSupportBtn = document.getElementById("contactSupportBtn"); 

// Adding event listener for the contactSupport Btn 
contactSupportBtn.addEventListener("click", (event) => {
     // Setting interval 
     setInterval(() => {
        window.location.href = "/dashboard/contactSupport"; 
    }, 500)
})

// Adding event listener for the submit btn 
submitBtn.addEventListener("click", (event) => {
    event.preventDefault(); 

    // Checking if the forms are correctly filled 
    // Checking if the firstname field was filled 
    if (!firstname.value) {
        Swal.fire({
            title: "Firstname is required...",  
            icon: "error", 
            text: "Firstname is missing, please type in your firstname...", 
            confirmPassword: "Okay...", 
        })

        // Stopping the script 
        return; 
    }

    // Checking if the lastname name field was filled 
    else if (!lastname.value) {
        Swal.fire({
            title: "Lastname is required...",  
            icon: "error", 
            text: "Lastname is missing, please type in your lastname... ", 
            confirmPassword: "Okay...", 
        })
                
        // Stopping the script 
        return;
    }

    // Checking if the phone number was filled 
    else if (!phoneNumber.value) {
        Swal.fire({
            title: "Phonenumber is required...",  
            icon: "error", 
            text: "Phonenumber is missing, please type in your phonenumber...", 
            confirmPassword: "Okay...", 
        })
                
        // Stopping the script 
        return;
    }

    // Checking if the email address was filled 
    else if (!emailAddress.value) {
        Swal.fire({
            title: "Email is required...",  
            icon: "error", 
            text: "Email is missing, please type in your email...", 
            confirmPassword: "Okay...", 
        })
                
        // Stopping the script 
        return;
    }

    // Checking if the password address was filled 
    else if (!countryOfResidence.value) {
        Swal.fire({
            title: "Country of residence is required...",  
            icon: "error", 
            text: "Country of residence is missing, please type in your country of residence...", 
            confirmPassword: "Okay...", 
        }) 
                
        // Stopping the script 
        return;
    }

    else {
        // Execute the block of code if all forms are filled 
        // Getting all the user's data 
        let userData = JSON.stringify({
            "firstname": firstname.value, 
            "lastname": lastname.value, 
            "phoneNumber": phoneNumber.value, 
            "emailAddress": emailAddress.value, 
            "countryOfResidence": countryOfResidence.value, 
        })

        // Sending the data to the backend server 
        fetch('/dashboard/editProfile', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json', 
            }, 
            body: userData
        }).then((response) => response.json())
        .then((responseData) => {
            // Handle the response data 
            // Handle the response data 
            if (responseData.status === "success") {
                Swal.fire({
                    title: "User information changed...", 
                    text: "The user information has been changed...", 
                    icon: "success", 
                    confirmButtonText: "Okay..", 
                })
        
                // stop the execution process 
                closeEditProfileBtn.click(); 

                // Setting interval 
                setInterval(() => {
                    window.location.reload(); 
                }, 1000)
            }
        })
    }
})

// Creating a function to open the modal 
let openModal = () => {
    modal.style.left = "0px"; 
}

// Creating a function to close the modal 
let closeModal = () => {
    modal.style.left = "-100%"
}

// Event listeners 
changePasswordBtn.addEventListener('click', openModal); 
closeModalBtn.addEventListener("click", closeModal); 

// Adding event listener for the changePasswordModalBtn 
editProfileBtn.addEventListener("click", (event) => {
    modalEditProfile.classList.add('open'); 
})

closeEditProfileBtn.addEventListener("click", (event) => {
    modalEditProfile.classList.remove('open')
})

// Adding event listeners for the changePasswordModalBtn 
changePasswordModalBtn.addEventListener("click", (event) => {
    // Checking if the old password field was filled 
    if (!oldPassword.value) {
        // Display the error message 
        Swal.fire({
            title: "Old password requred...", 
            text: "Old password missing...", 
            icon: "error", 
            confirmButtonText: "Okay...", 
        })

        // stop the execution process 
        return; 
    } 

    else if (!newPassword.value) {
        // Display the error message 
        Swal.fire({
            title: "New password is required...", 
            text: "New password missing...", 
            icon: "error", 
            confirmButtonText: "Okay...", 
        })

        // stop the execution process 
        return; 

    }

    // Getting the old password, and the new password 
    const oldPasswordValue = oldPassword.value; 
    const newPasswordValue = newPassword.value; 

    // Creating a json object from the new password and 
    // old password 
    let data = JSON.stringify({
        oldPassword: oldPasswordValue, 
        newPassword: newPasswordValue, 
    })
    
    // Sending a put request to the server 
    fetch("/dashboard/changePassword", {
        method: "PUT", 
        headers: {
            'Content-Type': 'application/json', 
        }, 
        body: data 
    })
    .then(response => response.json())
    .then(responseData => {
        // Handle the response data 
        if (responseData.status === "success") {
            Swal.fire({
                title: "Password Changed ...", 
                text: "The password is changed...", 
                icon: "success", 
                confirmButtonText: "Okay..", 
            })
    
            // stop the execution process 
            closeModalBtn.click(); 
        }

        else if (responseData.status === "error") {
            // On error 
            Swal.fire({
                title: "Error changing password ...", 
                text: `${responseData.message}`, 
                icon: "error", 
                confirmButtonText: "Okay..", 
            })

            // Stop the execution process 
            return; 
        }

        // 
        else if (responseData.status === "error-redirect") {
            alert("Redirect the user")
        }
    })
})
