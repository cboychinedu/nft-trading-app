// Degug 
console.log('Debug'); 

// Getting all the dom elements 
const email = document.getElementById("email"); 
const password = document.getElementById("password"); 
const verificationCode = document.getElementById("verificatonCode"); 
const enterCodeField = document.getElementById("enterCode"); 
const loginButton = document.getElementById("loginButton"); 
const randomNumber = document.getElementById("randomNumber")
const loaderDiv = document.getElementById("loaderDiv");
const mainNavBarDiv = document.getElementById("mainNavbarDiv"); 
const eyeLogo = document.getElementById("eyeLogo");  

// // Show the loader div when the page loads 
// document.addEventListener("DOMContentLoaded", () => {

//   mainNavBarDiv.style.display = "none";
//   loaderDiv.style.display = "flex";  

//   // Setting time out  
//   setTimeout(hideLoaderDiv, 800); 
// })

// // Creating a function for hiding the loader 
// let hideLoaderDiv = () => {
//   // Hiding the loader div 
//   loaderDiv.style.display = "none"; 
//   mainNavBarDiv.style.display = "flex"; 
// }

// Running the script on load 
let randomNumberResult = generateRandomNumber(); 
randomNumberResult = String(randomNumberResult); 

// Setting the eyeStatus to be false 
let eyeStatus = false;

// Adding event listener for the eye logo 
eyeLogo.addEventListener("click", (event) => {
  // Checking the eye status 
  if (eyeStatus === false ) {
      // Execute the following if the eyeStatus is false 
      password.setAttribute("type", "text"); 
      eyeStatus = true; 
  } 

  else {
    // Execute the following if the eyeStatus is true 
    password.setAttribute("type", "password"); 
    eyeStatus = false; 
  }
})

// Adding the event listener for the login button 
loginButton.addEventListener("click", (event) => {
    // Checking if the email field was filled 
    if (!email.value) {
        Swal.fire({
            title: "Email field is required...", 
            text: "Email address is missing", 
            icon: "error", 
            confirmButtonText: "Okay...", 
        })
    }

    // Checking if the password value is filled 
    else if (!password.value) {
        Swal.fire({
            title: "Password is required..", 
            text: "Password is required..", 
            icon: "error", 
            confirmButtonText: "Okay...", 
        })
    }

    // Checking if the entercode field was filled 
    else if (!enterCodeField.value) {
        Swal.fire({
            title: "Security code required...", 
            text: "Security code is required...", 
            icon: "error", 
            confirmButtonText: "Okay...", 
        })
    }

    // Checking if the random number generated is the same as that 
    // of the user typed in 
    else if (randomNumberResult != enterCodeField.value) {
      Swal.fire({
        title: "Security code not correct...", 
        text: "Security code is required is not correct...", 
        icon: "error", 
        confirmButtonText: "Okay...", 
      })
    }


    // Else all things go as planned!
    else {
        // Execute the block of code below 
        // Getting all the values 
        data = JSON.stringify({
            emailAddress: email.value, 
            password: password.value, 
            verificationCodeValue: enterCodeField.value,  

        })

        // Making a fetch request to the login server           
          fetch('/login', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: data
          }).then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Error: ' + response.status);
            }
          })
          .then(responseData => {
            // Handle the response data
            if (responseData.status === "success") {
                setTimeout(() => {
                  window.location.href = "/dashboard"; 
                }, 2000); 
            }  

            else if (responseData.status === "error") {
                // Execute the block of code below 
                setTimeout(() => {
                    // Using swal alert 
                    Swal.fire({
                      title: "Invalid Email or password", 
                      icon: "error", 
                      html: `<p class='swalpara'> ${responseData.message} </p>`, 
                      confirmPassword: "Okay...", 

                    })
                }, 2000); 
            }

          })
          .catch(error => {
            // Handle any errors
            console.error('Error:', error);
          });
          


    }
})
