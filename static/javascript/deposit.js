// Debug 
console.log("Deposit")

// Getting all the dom element 
const ethAddressValue = document.getElementById("ethAddressValue"); 
const copyAddressBtn = document.getElementById("copyAddressBtn"); 

// Adding event listener to copy the text 
copyAddressBtn.addEventListener("click", (event) => {
 // Create a range and select the text within the paragraph
    const range = document.createRange();
    range.selectNode(ethAddressValue);
    window.getSelection().removeAllRanges(); // Clear previous selections
    window.getSelection().addRange(range);
    
    // Copy the selected text to the clipboard
    document.execCommand("copy");
    
    // Deselect the text (optional)
    window.getSelection().removeAllRanges();
    
    // Alert the user that the text has been copied
    alert("Address copied");
    
})