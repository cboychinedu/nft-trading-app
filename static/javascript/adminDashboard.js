// Getting all the dom elements 
var menuButton = document.getElementById('menuButton');
var menuContainer = document.getElementById('menuContainer');
const closeLogo = document.getElementById("closeLogo"); 

// Opening the menu button 
menuButton.addEventListener('click', function() {
    menuContainer.classList.add('open');
});

// Closing the menu button 
closeLogo.addEventListener("click", (event) => {
    menuContainer.classList.remove("open"); 
})
