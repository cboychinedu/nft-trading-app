// Getting some necessary modules
let menuContainer = document.getElementById('dashboardMenuContainer');
let auctionsInnerDiv = document.getElementById("auctionsInnerDiv"); 
let auctionsOutterDiv = document.getElementById("auctionsOutterDiv");
let depositBtn = document.getElementById("depositBtn"); 
let withdrawBtn = document.getElementById("withdrawBtn"); 
let modalContent = document.getElementById("modalContent"); 
let modalScreen = document.getElementById("modalScreen"); 
let addLogo = document.getElementById("addLogo"); 


// Addin event listener for the add logo 
addLogo.addEventListener("click", (event) => {
    // Setting the modal Content 
    modalContent.style.display = "grid"; 
    modalScreen.style.display = "grid"; 

    // 
    setTimeout(() => {
        modalContent.style.opacity = '0';
        modalScreen.style.opacity = '0'; 
        
        // 
        setTimeout((e) => {
            // 
            modalContent.style.display = 'none'; 
            modalScreen.style.display = 'none'
        }, 300)
    }, 3000)
})


// Adding event listeners 
depositBtn.addEventListener("click", (event) => {
    setInterval(() => {
        window.location.href = "/dashboard/deposit"; 
    }, 1000)
})

// Adding event listeners for the withdraw button 
withdrawBtn.addEventListener("click", (event) => {
    setInterval(() => {
        window.location.href = "/dashboard/withdraw"; 
    }, 1000)
})

// Creating a function for creating a section 
let sectionFunction = (className=null, id=null) => {
    let section = document.createElement("section"); 
    section.classList.add(className); 
    section.id = id; 

    // Return the section 
    return section; 

}

// Creating a function for creating a div 
let divFunction = (className=null, id=null) => {
    let div = document.createElement("div"); 
    div.classList.add(className); 
    div.id = id

    // Return the div 
    return div; 
}

// Creating a function for creating the image 
let imageFunction = (src=null, className=null, id=null) => {
    // Creating the image tag 
    let image = document.createElement("img"); 
    image.classList.add(className); 
    image.src = src; 
    image.id = id 

    // Returning the image 
    return image; 
}


// creating a function for returning the acutions div
let auctionsMainDiv = (props) => {
    let div, img, span; 

    // Creating the outter div
    let acutionsDiv = document.createElement("div");
    acutionsDiv.classList.add("acutionsDiv")

    // Creating the nftImageDiv
    let nftImageDiv = document.createElement("div"); 
    nftImageDiv.classList.add("nftImageDiv"); 

    // Creating the image tag inside the nftimage div 
    let nftImageDisplay = document.createElement("img"); 
    nftImageDisplay.classList.add("nftImageDisplay"); 
    nftImageDisplay.src = "../uploads/imageData/img11.jpg"; 

    // Adding to then nftImageDiv 
    nftImageDiv.appendChild(nftImageDisplay); 
    acutionsDiv.appendChild(nftImageDiv); 

    // Creating the class views div 
    let viewsDiv = document.createElement("div"); 
    viewsDiv.classList.add("viewsDiv"); 

    // div 
    div = divFunction(null, null); 
    img = imageFunction("../images/dashboard/eye2.png", "viewsImageLogo"); 
    img.src = "../images/dashboard/eye2.png"; 

    // Appending the image 
    div.appendChild(img); 
    viewsDiv.appendChild(div); 

    // Div 
    div = divFunction(null, null); 
    span = document.createElement("span"); 
    span.innerText = "1.4M" + " Views"; 
    div.appendChild(span); 
    viewsDiv.appendChild(div); 

    // Appending into the main div 
    acutionsDiv.appendChild(viewsDiv); 

    // Creating the user name div 
    let userNameDiv = divFunction("userNameDiv", null); 
    userNameDiv.innerHTML = `
        <div> 
            <img src="../images/creativekillers.jpg" class="userLogo" /> 
        </div> 
        <div class="userNameDisplayDivPreview">
            <p> 
                Violet Eyes 
            </p> 
            <p> John stevens </p>  
        </div>
    `; 

    // Appending the userNamediv into the acution div 
    acutionsDiv.appendChild(userNameDiv); 

    // Creating the button sales 
    let buttonSalesDiv = divFunction("buttonSalesDiv", null); 
    let salesButton = document.createElement("button"); 
    salesButton.classList.add("salesButton"); 
    salesButton.id = "salesButton"; 
    salesButton.innerHTML = `
        <span class="currentBid"> Buy </span> 
        <span class="etherValueSales"> 0.45 ETH </span> 

    `
    buttonSalesDiv.appendChild(salesButton); 
    acutionsDiv.appendChild(buttonSalesDiv); 

    // 
    return acutionsDiv; 
    
}

// When the dom loads up
window.addEventListener('DOMContentLoaded', function() {
    // Getting the navbar dom elements
    const navHeight = document.querySelector('mainNavbarDiv');
    const stickyDiv = document.getElementById('stickyDiv');
    const menuSection = document.getElementById("menuSection");

    console.log("working")

    // Using Fetch request 
    fetch('/nftData', {
        method: 'GET'
    })
    // Convert the response data into a json object 
    .then(response => response.json())
    .then(data => { 
        // Get the grid container element 
        const gridContainer = document.getElementById('gridContainer'); 

        // Loop through the fetched data and create grid items 
        data.forEach(item => {
            let gridItem = auctionsMainDiv(item); 
            gridItem.classList.add("grid-item"); 
            gridContainer.appendChild(gridItem); 
        })
           
        })

        // console.log(auctionsInnerDivSection)
        console.log(data); 


    })

    // // Set the initial top value
    // stickyDiv.style.top = navHeight + 'px';
    // menuSection.style.top = navHeight + "px";

    // // Adjust the top value on window scroll
    // window.addEventListener('scroll', function() {
    //     stickyDiv.style.top = (navHeight - window.scrollY) + 'px';
    //     menuSection.style.top = (navHeight - window.scrollY) + 'px';

    // });

    // 
    // const auctions = auctionsMainDiv()
    // auctionsInnerDiv.appendChild(auctions)


























// // Importing the necessary dashboard
// // import { dashboardPageDisplay } from './dashboardPages/dashboardPage.js';
// // import { depositFunds } from './dashboardPages/deposit.js';

// // Getting the dom elements
// let menuButton = document.getElementById("menuButton");
// let menuContainer = document.getElementById("dasbboardMenuContainer");
// let livePreviewButton = document.getElementById("livePreviewButton");
// let nftMarketPlaceSideDiv = document.getElementById("nftMarketPlaceSideDiv");
// let dashboardContentDiv = document.getElementById("dashboardContentDiv");
// let dashboardSideDiv = document.getElementById("dashboardSideDiv");
// let landMarketPlaceSideDiv = document.getElementById("landMarketPlaceSideDiv");
// let resellNftSideDiv = document.getElementById("resellNftSideDiv");
// let resellLandSideDiv = document.getElementById("resellLandSideDiv");
// let withdrawSideDiv = document.getElementById("withdrawSideDiv");
// let profileSideDiv = document.getElementById("profileSideDiv");
// let createNftSideDiv = document.getElementById('createNftSideDiv');
// let contactSupportSideDiv = document.getElementById("contactSupportSideDiv");
// let createLandSideDiv = document.getElementById("createLandSideDiv");

// // Adding a click event for the createLandSideDiv
// createLandSideDiv.addEventListener('click', (event) => {
//     setTimeout(() => {
//         window.location.href = '/dashboard/createLand';
//     }, 1000)
// })

// // Adding a click event for the contactSupportSideDiv
// contactSupportSideDiv.addEventListener("click", (event) => {
//     setTimeout(() => {
//         window.location.href = '/dashboard/contactSupport';
//     }, 1000)
// })

// // Adding a click event to the createNftSideDiv
// createNftSideDiv.addEventListener('click', (event) => {
//     setTimeout(() => {
//         window.location.href = '/dashboard/createNft';
//     }, 1000)
// })

// // Adding a click event to the profileSideDiv
// profileSideDiv.addEventListener('click', (event) => {
//     setTimeout(() => {
//         window.location.href = '/dashboard/profile';
//     }, 1000)
// })

// // Adding a click event to the withdrawside div
// withdrawSideDiv.addEventListener('click', (event) => {
//     setTimeout(() => {
//         window.location.href = '/dashboard/withdraw';
//     }, 1000)
// })

// // Adding a click event to the depositDiv
// depositSideDiv.addEventListener('click', (event) => {
//     setTimeout(() => {
//         window.location.href = '/dashboard/deposit';
//     }, 1000)
// })

// // Adding a click event to the resellLandSideDiv
// resellLandSideDiv.addEventListener("click", (event) => {
//     setTimeout(() => {
//         window.location.href = '/dashboard/resellLand'
//     }, 1000)
// })

// // Adding a click event to the resellNftSideDiv
// resellNftSideDiv.addEventListener("click", (event) => {
//     setTimeout(() => {
//         window.location.href = '/dashboard/resellNft';
//     }, 1000)
// })

// // Adding a click event to the dashboard
// dashboardSideDiv.addEventListener("click", (event) => {
//     // Redirect to another page after 2 seconds
//     setTimeout(() => {
//         window.location.href = '/dashboard';
//     }, 1000);
// });

// // Adding a click event to the nft market place side div
// nftMarketPlaceSideDiv.addEventListener("click", (event) => {
//     // Redirect to another page after 2 seconds
//     setTimeout(() => {
//         window.location.href = '/dashboard/nftMarketPlace';
//     }, 1000);
// });

// // Adding a click event to the nft land market places side div
// landMarketPlaceSideDiv.addEventListener("click", (event) => {
//     // Redirect to another page after 1 seconds
//     setTimeout(() => {
//         window.location.href = '/dashboard/landMarketPlace';
//     }, 1000);
// })

// // Setting the menu open status to false
// var isMenuOpen = false;

// // Adding event listeners for the menu button
// menuButton.addEventListener('click', function() {
//     isMenuOpen = !isMenuOpen;
//     // if the menu not open is true, execute the block of code below
//     if (isMenuOpen) {
//         // Add the open class tag to the class list
//         menuContainer.classList.add('open');
//     } else {
//         // Else, remove the open class list
//         menuContainer.classList.remove('open');
//     }
// });

// // Add an event listener for mouse leave when the user is done looking
// // at the menu container
// menuContainer.addEventListener('mouseleave', function() {
//     if (isMenuOpen) {
//         menuContainer.classList.remove('open');
//         isMenuOpen = false;
//     }
// });
