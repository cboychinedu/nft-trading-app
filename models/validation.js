// Importing the necessary modules 
const mongodb = require('mongoose'); 

// Creating a schema for the users 
const usersSchema = new mongodb.Schema({
    firstname: { type: String, required: true, minlength: 3, maxlength: 50 }, 
    lastname: { type: String }, 
    phoneNumber: { type: Number, default: "", minlength: 4 }, 
    emailAddress: { type: String, required: true, minlength: 3 }, 
    password: { type: String, required: true, minlength: 3 }, 
    accountBalance: { type: String, required: true, default: "0.05345" },
    dateLastLoggedIn: { type: String }, 
    countryOfResidence: { type: String }, 
    verificationStatus: {type: String, default: "false" },  
    // dataTwo: { type: Buffer , default: Buffer.from([40, 30, 100])}, 
    date: { type: Date, default: Date.now }
})

// Creating a schema for the admin user's 
const adminSchema = new mongodb.Schema ({
    firstname: { type: String, required: true, minlength: 3, maxlength: 50 }, 
    lastname: { type: String }, 
    emailAddress: { type: String, required: true, minlength: 3 }, 
    password: { type: String, required: true, minlength: 3 }, 
    date: { type: Date, default: Date.now } 
})

// Creating a schema for the uploaded nft collection 
const uploadedNftBlobFile = new mongodb.Schema({
    firstname: { type: String, required: true }, 
    lastname: { type: String, required: true }, 
    emailAddress: { type: String, required: true }, 
    nftTitle: { type: String, required: true}, 
    nftArtistName: { type: String, }, 
    nftCategory: { type: String, }, 
    nftPrice: { type: String, required: true, default: "0.00" }, 
    nftImageFileLocation: { type: String }, 
    nftDescription: { type: String, required: true }, 
    saleStatus: { type: Boolean, default: false }, 
    date: { type: Date, default: Date.now } 

})


// Connecting to the user's collection 
const USERS = mongodb.model('users', usersSchema); 
const ADMIN = mongodb.model('admins', adminSchema); 
const NFTFILE = mongodb.model('nftfiles', uploadedNftBlobFile); 


// Exporting the modules 
module.exports.USERS = USERS; 
module.exports.ADMIN = ADMIN; 
module.exports.NFTFILE = NFTFILE; 