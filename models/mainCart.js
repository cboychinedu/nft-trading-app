// Importing the necessary modules 
const mongodb = require('mongoose');

// 
// Creating a schema for the carts 
const cartItemSchema = new mongodb.Schema({
    firstname: { type: String, required: true }, 
    lastname: { type: String, required: true }, 
    emailAddress: { type: String, required: true }, 
    nftTitle: { type: String, required: true}, 
    nftArtistName: { type: String, }, 
    nftCategory: { type: String, }, 
    nftPrice: { type: String, required: true, default: "0.00" }, 
    nftImageFileLocation: { type: String }, 
    nftDescription: { type: String, required: true }, 
    saleStatus: { type: Boolean, default: false }, 
    date: { type: Date, default: Date.now } 
})

// Creating the main cart schema 
const mainCartSchema = new mongodb.Schema({
    items: [cartItemSchema], 
    saleStatus: { type: Boolean, default: false },  
}); 

// Connecting to the mainCart's collection 
const MAINCARTITEM = mongodb.model('maincartitem', mainCartSchema); 

// Exporting the mainCart's module 
module.exports.MAINCARTITEM = MAINCARTITEM;